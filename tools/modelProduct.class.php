<?php
class modelProduct{
	
	private $api = null;
	private $base = 'ecommerce';
	private $lang = 'pt_br';
	private $filters = null;
	private $category = null;
	private $maker = null;
	private $search = null;
	private $hasFilterList = null;
	private $hasMakerOptions = null;
	
	public function __construct() {
		$this->api = new API();
		$this->filters = array();
		$this->hasFilterList = true;
		$this->hasMakerOptions = true;
	}
	
	public function enableFilterList($bool) {
		$this->hasFilterList = $bool;
	}
	
	public function enableMakerOptions($bool) {
		$this->hasMakerOptions = $bool;
	}
	
	public function setMaker($maker){
		$this->maker = $maker;
	}
	
	public function setSearch($search){
		$this->search = $search;
	}
	
	public function setCategory($category){
		$this->category = $category;
	}
	
	public function addFilter($filter_item_int_id){
		$this->filters[] = $filter_item_int_id;
	}
	
	public function setFilters($filters){
		$this->filters = $filters;
	}
	
	public function getAll($page, $limit = 24){
		$data = new stdClass();
				
		$current_page = $page ? $page : 1;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		
		if($this->category == 0)
			$this->category = null;
		
		if(!empty($this->category)):
			$params = array('chr_language'=>$this->lang, 'category_int_id'=>$this->category);
			$this->api->addAction($this->base,'ec_get_category','run', $params);
		endif;
		
		foreach($this->filters as $f)
			$this->api->addAction($this->base,'ec_set_filter_item','run', array('filter_item_int_id'=>$f));
		
		$params = array(
			'product_int_category'=>$this->category, 
			'product_int_product_maker'=>$this->maker,
			'chr_language'=>$this->lang,
			'vrc_search'=>$this->search
		);
		$this->api->addAction($this->base,'ec_set_tmp_product_sub_filter_item','run', $params);
		
		if($this->hasFilterList):
			$params = array('chr_language'=>$this->lang);
			$this->api->addAction($this->base,'ec_list_filter_item_after_filter','run', $params);
			$this->api->addAction($this->base,'ec_list_filter_item_active_after_filter','run', $params);
		endif;
		
		if($this->hasMakerOptions):
			$this->api->addAction($this->base,'ec_list_product_maker_options','run', array());
		endif;
		
		$params = array('chr_language'=>$this->lang, 'int_start'=>$start, 'int_limit'=>$limit );
		$this->api->addAction($this->base,'ec_list_product_after_filter','run', $params);

		$responses = $this->api->callMethodCombo();
		
		$data->category = null;
		if(!empty($this->category)):
			$category = array_shift($responses);
			$data->category = current($category->data);
		endif;
		
		foreach($this->filters as $f)
			array_shift($responses);
		
		array_shift($responses);
		
		$data->filters = $data->active_filters = $data->makers = array();
		if($this->hasFilterList):
			$options_filters = array_shift($responses);
			$active_filters = array_shift($responses);
			$data->filters = $options_filters->data;
			$data->active_filters = $active_filters->data;
		endif;
		
		if($this->hasMakerOptions):
			$makers = array_shift($responses);
			$data->makers = $makers->data;
		endif;
		
		$products = array_shift($responses);
		$data->products = $products->data;
		$data->limit = $limit;
		$data->records = !$products->data ? 0 : current((array)$products->data)->total_records;
		
		$data->pag = new Pagination($current_page, $limit, $data->records);
		
		
		return $data;
	}
	
	public function getOne($sid){
		$params = array('product_sub_int_id'=>$sid);
		$this->api->addAction('ecommerce','ec_get_product_sub','run', $params);
		$subproduct = array_shift($this->api->callMethod()->data);
		$res = new stdClass();
		$res->subproduct = $subproduct;
		$id = $subproduct->product_sub_int_product;
		$params = array('int_product'=>$id, 'chr_language'=>$this->lang);
		$this->api->addAction('ecommerce','ec_get_product','run', $params);
		$res->data = current($this->api->callMethod()->data);
		
		$params = array( 'product_sub_int_id' => $sid );
		$this->api->addAction('ecommerce','ec_get_product_sub','run', $params);
		
		$params = array( 'int_product_sub' => $sid );
		$this->api->addAction('ecommerce','ec_get_product_sub_current_price','run', $params);

		$params = array( 'int_product' => $id );
		$this->api->addAction('ecommerce','ec_list_product_description','run', $params);

		$params = array( 'int_product' => $id );
		$this->api->addAction('ecommerce','ec_list_product_attribute','run', $params);

		$params = array( 'int_product' => $id );
		$this->api->addAction('ecommerce','ec_list_product_image','run', $params);

		$this->api->addAction('ecommerce','ec_list_product_sub_option_item','run', array( 'product_int_id'=>$id, 'product_sub_int_id'=>$sid, 'chr_language'=>'pt_br'));
		
		$this->api->addAction('ecommerce','ec_list_product_sub','run', array( 'int_product' => $id, 'chr_language'=>$this->lang));

		// Executa todas as requisiçoes na API e retorna a lista de repostas
		$exec = $this->api->callMethodCombo();
		
		$res->id = $id;
		$res->sub = current(array_shift($exec)->data);
		$res->data_price = current(array_shift($exec)->data);
		$res->desc = array_shift($exec)->data[0];
		$res->attr_data_list = array_shift($exec)->data;
		$res->img_data_list = array_shift($exec)->data;
		$product_options = array_shift($exec)->data;
		$res->subproducts = array_shift($exec)->data;
		
		$res->select_subproducts = array();
		
		foreach($res->subproducts as $s)
			$res->select_subproducts[] = array(
				H::link('product', URL::build($s->name, $s->product_sub_int_id)), 
				sprintf('%sCÓD %s' ,(strlen($s->filter) > 0 ? $s->filter . ', ' : ''), $s->product_sub_vrc_code)
			);
		
		$res->options_groups = array();
		$g = -1;
		$label = null;
		foreach($product_options as $o):
			if(empty($o->checked))
				continue;

			if($o->label != $label):
				$g++;
				$label = $o->label;
				$res->options_groups[$g] = new stdClass();
				$res->options_groups[$g]->label = $label;
				$res->options_groups[$g]->options = array();
			endif;
			
			$res->options_groups[$g]->options[] = array( $o->item_id, $o->value);
		endforeach;
		
		return $res;
	}
}