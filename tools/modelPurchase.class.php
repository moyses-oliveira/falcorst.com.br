<?php
class modelPurchase {
	
	public static function getPurchaseByHash($purchase_vrc_hash) {
		$api = new API();
		$api->addAction('ecommerce', 'ec_get_purchase_by_hash', 'run', array('purchase_vrc_hash'=>$purchase_vrc_hash));
		$res = $api->callMethod();
		
		if(!!$res->errors):
			echo URL::er500();
			die;
		endif;
		
		return current($res->data);
	}
	public static function getInfo($purchaseId, &$data) {
		# Compra
		$api = new API();
		$api->addAction('ecommerce', 'ec_get_purchase', 'run', array('purchase_int_id' => $purchaseId,'customer_int_id' => MY_ID));
		$api->addAction('ecommerce', 'ec_list_purchase_item', 'run', array('purchase_int_id' => $purchaseId));
		$api->addAction('ecommerce', 'ec_list_purchase_method', 'run', array('purchase_method_int_purchase' => $purchaseId));
		$api->addAction('ecommerce', 'ec_list_purchase_comment', 'run', array('purchase_int_id' => $purchaseId));
		
		$res = $api->callMethodCombo();
		
		foreach($res as $r):
			if(!!$r->errors):
				echo URL::er500();
				die;
			endif;
		endforeach;
		
		$data->purchase = current($res[0]->data);
		$data->items = $res[1]->data; 
		$data->methods = $res[2]->data; 
		$data->address = $data->purchase;
		$data->comments = $res[3]->data;
	}
	
	public static function refreshStatusByGatewayNotification($settings) {
		if(!isset($_REQUEST['gateway']))
			return null;
		
		if(!isset($_REQUEST['cancel'])):
			$purchase_status_vrc_alias = static::getStatus($settings, $_REQUEST['gateway']);
		else:
			$purchase_status_vrc_alias = 'payment_denied';
		endif;
		
		if(!$purchase_status_vrc_alias)
			return null;
		
		return static::setStatusByHash($purchase_status_vrc_alias);
	}
	
	public static function setStatusByHash($purchase_status_vrc_alias){
		$params = array(
				'purchase_vrc_hash' => $_REQUEST['hash'],
				'purchase_status_vrc_alias' => $purchase_status_vrc_alias
		);
		$api = new API();		
		$api->addAction('ecommerce', 'purchase_change_status_by_hash', 'run', $params);
		$res = $api->callMethod();
		
		$errors = $res->errors;
		if(!!$errors)
			return null;
		
		return current($res->data)->res;
	}
	
	
	private static function getStatus($settings, $gateway) {
		if(isset($_REQUEST['hash']) &&  $gateway == 'mercadopago'):
			$purchase = static::getPurchaseByHash($_REQUEST['hash']);
			$GLOBALS['external_reference'] = str_pad($purchase->purchase_int_id, 13, '0', STR_PAD_LEFT);
		endif;
		
		$cred =	static::getCredentials($gateway, $settings);
		$paymentGateway = new paymentGateway($gateway, $cred, $cred['sandbox']);
		
		$statusID = $paymentGateway->notificationStatus();
		
		switch($gateway) {
			case 'pagseguro':
				
				if($cred['sandbox']):
					header('Access-Control-Allow-Origin: https://sandbox.pagseguro.uol.com.br');
					header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
					header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
					header('Access-Control-Allow-Credentials: true');
				endif;
				switch($statusID)
				{
					case '1':
					case '2':
					case '5':
						return 'payment_check';
					case '3':
					case '4':
						return 'payment_accepted';
					case '6':
					case '7':
						return 'payment_denied';
					default:
						return null;
				}
			case 'mercadopago':
				switch($statusID)
				{
					case 'pending': # The user has not yet completed the payment process
					case 'in_process': # Payment is being reviewed
					case 'in_mediation': # Users have initiated a dispute
						return 'payment_check';
					case 'approved': # The payment has been approved and accredited
						return 'payment_accepted';
					case 'rejected': # Payment was rejected. The user may retry payment.
					case 'refunded': # Payment was refunded to the user
					case 'cancelled': # Payment was cancelled by one of the parties or because time for payment has expired
					case 'charged_back': # Was made a chargeback in the buyer’s credit card
						return 'payment_denied';
					default:
						return null;
				}
			default:
				return null;
		}
	
	}
	
	
	public static function getCheckoutURL(
		$settings, # Configurações do site
		$gateway,  # Alias do gateway de pagamento
		$purchaseHash, # ID hash da compra
		$purchaseId,  # ID hash da compra 
		$cartItems, # Items do carrinho
		$address, # Endereço do destinatário
		$shippingMethod, # Método de entrega
		$shippingPrice # Valor da entrega
	) {
		try
		{
			$cred =	static::getCredentials($gateway, $settings);
			
			$paymentGateway = new paymentGateway($gateway, $cred, $cred['sandbox']);
			
			$paymentRequest = new paymentRequest();	

			# Definiçao do comprador - Metodo 1
			# ----------------------------------------------------
			$paymentRequest->setSenderName('Cliente: '.MY_NAME);
			$paymentRequest->setSenderEmail(MY_EMAIL); 
			
			
			foreach($cartItems as $i)
				$paymentRequest->addItem($i->product_sub_int_id, $i->name, round($i->quantity), $i->price);
			
			$paymentAddr = new Address();
			$paymentAddr->setPostalCode(str_replace(array('.','-'),'',$address->address_vrc_postcode));  
			$paymentAddr->setCountry($address->address_vrc_country_code);  	
			$paymentAddr->setState($address->address_vrc_zone);  
			$paymentAddr->setCity($address->address_vrc_city);  
			$paymentAddr->setDistrict($address->address_vrc_neighborhood);  
			$paymentAddr->setStreet($address->address_vrc_address);  
			$paymentAddr->setNumber($address->address_vrc_number);  
			$paymentAddr->setComplement($address->address_vrc_complement);   
			
			$shippingType = new ShippingType();
			$shippingMethod= in_array($shippingMethod ,array('PAC','SEDEX')) ? $shippingMethod : 'NOT_SPECIFIED';
			
			$shippingType->setByType($shippingMethod); 
			$confShipping = new Shipping(); 
			$confShipping->setAddress($paymentAddr);   
			$confShipping->setType($shippingType);
			$confShipping->setCost(number_format($shippingPrice * 1, 2, '.', ''));
			$paymentRequest->setShipping($confShipping);
			
			$paymentRequest->setCurrency("BRL");
			$paymentRequest->setReference(str_pad($purchaseId, 13, '0', STR_PAD_LEFT));
			
			$url = $_SERVER['REQUEST_SCHEME'] . ':' . URL::site();
			$gpnURL = sprintf('%purchase/gateway-notification/?hash=%s&gateway=%s', $url, $purchaseHash, $gateway);
			$paymentRequest->setCancellationURL($gpnURL . '&cancel=1');
			$paymentRequest->setNotificationURL($gpnURL);
			$paymentRequest->setRedirectURL($url . 'purchase/step-5/' . $purchaseId . '/' . $gateway);
			$gatewayResponse = $paymentGateway->register($paymentRequest);

			return $gatewayResponse->checkout_url;
		}
		catch(Exception $e)
		{
			echo $e->getMessage();die;
			return false;
		}
	}
	
	public static function getCredentials($gateway, $settings){
		switch($gateway) {
			case 'pagseguro':
				return array(
					"client" => $settings->pagseguro_email,
					"token" => $settings->pagseguro_token,
					"sandbox" => $settings->pagseguro_sandbox == 's'
				);
			case 'mercadopago':
				return array(
					"client" => $settings->mercadopago_client,
					"token" => $settings->mercadopago_token,
					"sandbox" => false
				);
			default:
				return null;
		}
	}
	
}