<?php
class dataEcommerce {
	public static function load(&$data){
		if(isset($_SESSION['customer'])):
			$constants = array('MY_ID'=>'customer_int_id','MY_NAME'=>'customer_vrc_name','MY_EMAIL'=>'customer_vrc_email');
			foreach($constants as $constant=>$k):
				define($constant,$_SESSION['customer']->{$k});
			endforeach;
			define('CART_ID', null);
		else:
			if(isset($_COOKIE['CART_ID'])):
				$cart_id = $_COOKIE['CART_ID']; 
			else:
				$cart_id = base_convert(md5(uniqid()), 16, 36) . base_convert(mt_rand(0,pow(10,10)), 10, 36);
			endif;
			setcookie('CART_ID', $cart_id, strtotime('+6 hours'),'/');
			define('CART_ID', $cart_id); 
		endif;
		
		
		$cartItems = modelCart::getItems();
		$data->header_search_input = null;
		$data->shopping_cart_data = new stdClass();
		$data->shopping_cart_data->items = $cartItems;
		$data->shopping_cart_data->length = count($cartItems);
		$total = 0;
		foreach($data->shopping_cart_data->items as &$i):
			$subtotal = $i->price * $i->quantity;
			$i->subtotal =  number_format($subtotal, 2, ',', '.');
			$i->price = number_format($i->price, 2, ',', '.');
			$i->quantity = number_format($i->quantity, 0, '', '.');
			$path = substr($i->img, 0,-1 * strlen(basename($i->img)));
			$i->thumb = H::root() . ImagePlugin::resize(PATH_IMAGES . $i->img, trim(PATH_THUMBS . $path,'/'), 94, 74, true);
			$i->url = H::link('product', URL::build($i->name, $i->product_sub_int_id));
			$total +=  $subtotal;
		endforeach;
		$data->shopping_cart_data->total = number_format($total, 2, ',', '.');
		
		$modelCat = new modelCategory();
		$data->catTree = $modelCat->getTree();
		
	}
}