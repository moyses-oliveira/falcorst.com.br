<?php
class modelCMS {
	private $api = null;
	public function __construct(){
		$this->api = new API();
	}
	
	public function findWeblogPost($alias, $id = null, $language='pt_br'){
		
		if(!!$id):
			$params = array(
				'postd_int_post'=>$id,
				'wbl_vrc_alias'=>$alias, 
				'postd_chr_language'=>$language
			);
			$this->api->addAction('cms','cms_get_weblog_post_description_by_alias','run', $params);
		else:
			$params = array(
				'int_start' => 0,
				'int_limit' => 1, 
				'vrc_name' => null, 
				'vrc_alias' => $alias, 
				'chr_language' => $language
			);
			$this->api->addAction('cms','cms_list_weblog_post','run', $params);
		endif;
	}
	
	public function execAllGetFirst(){
		$result = $this->api->callMethodCombo();
		$responses = array();
		foreach($result as $r):
			if(!isset($r->data[0])):
				throw new Exception('Erro ao carregar os dados.');
			endif;
			$responses[] = $r->data[0];
		endforeach;
		return $responses;
	}
	
	public function getApi(){
		return $this->api;
	}
}