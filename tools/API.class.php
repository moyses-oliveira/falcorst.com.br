<?php
class API {
	private $data = null;
	public function __construct(){ $this->data = array(); }
	
	public function addAction($schema, $structure, $action, $data = array()) { 
		$this->data[] =
			array(
				'schema' => $schema, 'structure' => $structure, 'action' => $action, 'data'=>$data
			);
	}
	
	public function clearActions() { $this->data = array(); }

	public function callMethod() {
		return $this->call();
	}
	
	public function callMethodCombo() {
		return $this->call(true);
	}
	
	private function call($build_list = false) {
		//open connection
		$post = array('data'=>json_encode($this->data));
		$this->clearActions();
		$ch = curl_init();
		//set the url, number of POST vars, POST data
		$get = !$build_list ? false : array('build_list'=>$build_list);
		$get = !$get ? '' : '?'. http_build_query($get);
		$url = API_URL . API_TOKEN . $get;
		curl_setopt($ch, CURLOPT_URL, $url);
		$fields_string = !$post ? null : http_build_query($post);
		$total_fields = !$fields_string ? 0 : substr_count($fields_string,'&') + 1;
		curl_setopt($ch, CURLOPT_POST, $total_fields);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);

		//execute post
		$json = curl_exec($ch);
		//close connection
		curl_close($ch);
		$response = json_decode($json);
		if(!is_object($response) && !is_array($response)) { echo $json;return; }
			#return URL::er500(array('Message'=>'Cannot decode json string.','API Returns'=>$json));
		
		if(!is_array($response)):
			foreach(array('errors') as $k)
				$response->{$k} = (array)$response->{$k};
		endif;
		return $response;
	}
}