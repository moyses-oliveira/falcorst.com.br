<?php
class modelPeople {
	public static function find($start=0, $limit=10000, $search=null){
		$api = new API();
		$params = array(
			'int_start'=>$start,
			'int_limit'=>$limit, 
			'vrc_name'=>$search
		);
		$api->addAction('cms','cms_list_people','run', $params);
		return $api->callMethod()->data;
	}
}