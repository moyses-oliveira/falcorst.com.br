<?php
class TPL {
	
	public static function format($tpl, $data) {
		
		$from = array();
		$to = array();
		foreach($data as $k=>$v):
			if(is_object($v) || is_array($v)) continue;
			$from[] = '{'.$k . '}';
			$to[] = $v;
		endforeach;
		
		return str_replace($from, $to, $tpl);
	
	}
}