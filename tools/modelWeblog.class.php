<?php
class modelWeblog {
	public static function posts($alias, $start, $limit, $language='pt_br', $search=null){
		$api = new API();
		$params = array(
			'int_start'=>$start,
			'int_limit'=>$limit, 
			'vrc_name'=>$search, 
			'vrc_alias'=>$alias, 
			'chr_language'=>$language
		);
		$api->addAction('cms','cms_list_weblog_post','run', $params);
		return $api->callMethod()->data;
	}
	
	public static function images($int_post){
		$api = new API();
		$params = array(
			'int_post'=>$int_post
		);
		$api->addAction('cms','cms_list_weblog_post_image','run', $params);
		return $api->callMethod()->data;
	}
	
	public static function post($alias, $id = null, $language='pt_br'){
		$model = new modelCMS();
		$model->findWeblogPost($alias, $id = null, $language='pt_br');
		$data = $model->getApi()->callMethod()->data;
		if(count($data)):
			return $data[0];
		endif;
		return (object)array(
				'post_int_id'=>null,
				'post_int_wblcat'=>null, 
				'category'=>null,
				'postd_vrc_title'=>null, 
				'postd_vrc_summary'=>null, 
				'postd_txt_description'=>null, 
				'postd_chr_language'=>null
			);
	}
}