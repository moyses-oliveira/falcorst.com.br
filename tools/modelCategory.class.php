<?php
class modelCategory {
	
	
	private $api = null;
	private $base = 'ecommerce';
	private $lang = 'pt_br';
	
	public function __construct() {
		$this->api = new API();
	}
	
	public function getTree(){
		$api = new API();
		$params = array('chr_language'=>$this->lang);
		$api->addAction($this->base,'ec_list_category_tree','run', $params);
		$data = $api->callMethod()->data;
		$tree = array();
		$parent = null;
		foreach($data as $k=>$d):
			if(empty($d->pid)):
				if(!empty($parent))
					$tree[] = $parent;
				
				$parent = $d;
				$parent->children = array();
			else:
				$parent->children[] = $d;
			endif;
		endforeach;
		if(!empty($parent))
			$tree[] = $parent;
		
		return $tree;
	}
}