<?php
class modelCart {
	
	public static function getItems(){
		$api = new API();
		$customer_int_id = defined('MY_ID') ? MY_ID : null;
		$vrc_anonimous = defined('CART_ID') ? CART_ID : null;
		$chr_language = 'pt_br';
		$params =	compact(array('customer_int_id', 'vrc_anonimous', 'chr_language'));
		$api->addAction('ecommerce', 'ec_list_cart_item', 'run', $params);
		$res = $api->callMethod();
		
		$errors = $res->errors;
		
		if(!!$errors):
			URL::er500();
			die;
		endif;
		
		return $res->data;
	}
}