<div id="page-header">
				
	<div class="container">
		<div class="row">
			<div class="span12">
				<h1 style="color: #fff;"><?php echo $_title;?></h1>
			</div><!-- end .span12 -->
			
		</div><!-- end .row -->
	</div><!-- end .container -->
	
</div>
<div class="container">
	<div class="row">
		<?php 
		$tpl = '
		<div class="span4">
			<div class="icon-box-1">
				<i class="%s"></i>
				<div class="icon-box-content">
					<h5><a href="%s">%s</a></h5>
					<p>%s</p>
				</div>
			</div>
		</div>';
		foreach($data as $k=>$r):
			if($k%3 == 0) echo '<div class="clear"></div>';
			$icon = $icons[$k];
			$alias = $aliases[$k];
			extract((array)$r);
			printf($tpl, $icon, H::link('servicos',$alias), $postd_vrc_title, $postd_vrc_summary);
		endforeach;
		?>
	</div><!-- end .row -->
</div>