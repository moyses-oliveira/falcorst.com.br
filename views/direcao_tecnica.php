<div id="page-header">
				
	<div class="container">
		<div class="row">
			<div class="span12">
				<h1 style="color: #fff;"><?php echo $_title;?></h1>
			</div><!-- end .span12 -->
			
		</div><!-- end .row -->
	</div><!-- end .container -->
	
</div>

<div class="container">
		<div class="row">
			<div class="span4 text-center">
				<?php 
					$img = H::root() . 'files/gallery/images/' . $data->image; 
					printf('<img src="%1$s" alt="%2$s" title="%2$s" />', $img, $data->postd_vrc_title);
				?>
			</div>		
			<div class="span8">
				<?php echo $data->postd_txt_description;?>
			</div>		
		</div><!-- end .row -->
	</div>


<div class="container">
	<div class="row">
		
	</div><!-- end .row -->
</div><!-- end .container -->