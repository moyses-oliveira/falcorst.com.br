

<?php 
if(defined('MY_ID')):
$user = substr(MY_NAME, 0, strpos(MY_NAME, ' '));
	printf('<h3 class="main-heading text-center">Produtos adquiridos até agora.</h3>');
	
	
	$thumb_path = 'thumbs/'; 
	$img_path = 'images/'; 
	$item_tpl = ' <div class="product-item col-md-3 col-sm-4 col-xs-6">
		<a href="{url_produto}/{id}" class="thumb" style=" background-image: url({thumb});">&nbsp;</a> 
		<div class="name"><a href="{url_produto}/{id}" class="">{name}</a></div>
		<div class="clear"></div>
	</div>
	';  
	echo '<div class="panel-body container-catalog">';

	if(count((array)$list_product)): 
		foreach($list_product as $data): 
			$path = substr($data->img, 0,-1 * strlen(basename($data->img))); 
			$thumb = H::root() . ImagePlugin::resize($img_path . $data->img, trim($thumb_path . $path,'/'), 180, 135, false); 
			echo  str_replace(
				array('{thumb}','{name}','{url_produto}','{id}'), 
				array($thumb, $data->name,URL_PRODUTO, URL::build($data->name, $data->id)), 
				$item_tpl
			); 
		endforeach; 
		echo  '<div class="clear"></div>'; 
	else:
		echo  '
		<h4 class="text-center">Nenhum produto foi adquirido.</h4>
		'; 
	endif;
	echo '</div>';

else:
	echo '<div class="col-md-5" id="content-login">';
	include('form-login.php');
	echo '</div>';
	echo '<div class="col-md-7" id="content-register">';
	include('form-register.php');
	echo '</div>';
endif;
