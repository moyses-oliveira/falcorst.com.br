<?php 
if(count($_POST) && !$errors):
	echo '<script type="text/javascript"> window.location.href = URL_CURRENT; </script>';
else: 
?>
	<div class="field form-group">
		<p>Se você já cadastrou uma conta, coloque os dados da conta abaixo:</p>
	</div>
	<?php include('form-warnings.php');?>
	<form action="<?php echo URL::link('customer','login', (isset($_GET['to']) ? '?to='.$_GET['to']  : '')  );?>" enctype="multipart/form-data" method="post" class="default-form form-horizontal update_target" id="form-login" target="#content-login">
		<?php 
			$id_prefix = 'field-login';
			foreach($fieldsLogin as $k=>$f):
				$type = in_array('pass', $f->type) ? 'password' : 'text';
				$class = implode(' ', $f->type);
				printf('
					<div class="form-group">
						<label for="%1$s-%2$s" class="col-sm-3 control-label">%3$s:</label>
						<input id="%1$s-%2$s" type="%4$s" name="%2$s" class="%5$s form-control col-sm-9" value="%6$s" />
					</div>
					', 
					$id_prefix, $k, $f->label, $type, $class, $f->value);
			endforeach;
			$refreshLink = sprintf('%s:%scustomer/refresh-password%s',
			$_SERVER['REQUEST_SCHEME'],URL::site(), (isset($_GET['to'])? '/?to='.$_GET['to'] : ''));
			
			printf('
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
					<button type="submit" class="btn btn-primary btn-block login">
						<i class="fa fa-sign-in"></i> &nbsp;Login
					</button>
				</div> 
			</div>', 
			H::link('customer','register')
			);
			printf('
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
						<a class="remember btn btn-danger btn-block" href="%s" >
							<i class="fa fa-question-circle"></i> &nbsp;Esqueceu a senha?
						</a>
				</div> 
			</div>', 
			$refreshLink
			);
		?>
	</form>
	<script type="text/javascript">
	$(document).ready(function(){
		$('#retry-confirm').click(function(){
			var tmp_target = '#confirm-warning';
			var tmp_options = {type: 'GET', url: $(this).attr('href')};
			$(tmp_target).html("<style='color: #A00;'>Aguarde, o e-mail está sendo enviado!</style>");
			tmp_options.success = function (html) {
				var html_target = $('#form-login').attr('target');
				$(html_target).html(html);
				$('#form-register-confirm').attr('target', '#content-login');
				globalFormConfig(html_target);
			};
			$.ajax(tmp_options);
			return false;
		});
		
		$('a.remember').click(function(){
			if(!validator.fieldValidate('#field-login-vrc_email')) {
				validator.showErrorBox('#form-login', ['Digite um e-mail válido para recuperar sua senha.']);
				return false;
			}
			var tmp_target = '#content-login';
			var tmp_options = { type: 'POST', url: $(this).attr('href'), data: { vrc_email: $('#field-login-vrc_email').val() }, dataType: 'json' };
			
			validator.showErrorBox('#form-login', ['Aguarde.']);
			tmp_options.success = function (json) {
				validator.showErrorBox('#form-login', [json.response]);
			};
			$.ajax(tmp_options);
			return false;
		});
	});
	</script>
<?php endif; ?>