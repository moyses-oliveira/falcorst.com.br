<?php
$type = !$errors ? 'success' : 'danger';
$msg = !$errors ? 'Você já está automaticamente logado ao sistema e será redirecionado em' : current($errors) . ' Você será redirecionado em';

?>
<div class="alert alert-<?php echo $type;?> alert-dismissable text-center">
	<!-- i class="fa fa-warning"></i -->
	<?php 
	if(!$errors):
		echo '
		<h3 style="color: #222;text-align: center;"><b>Parabéns!</b></h3>
		<h4 style="color: #222;text-align: center;">Seu cadastro foi realizado com sucesso.</h4>';
	else:
		echo '<h3 style="color: #222;text-align: center;"><b>Atenção!</b></h3>';
	endif;
	?>
	<p style="color: #222;text-align: center;"><?php echo $msg;?> <span class="n-seconds">5 segundos</span>.</p>
	<br/>
</div>
<script type="text/javascript">
	var count_n_sec = 5;
	function redirect_timer() {
		if(count_n_sec < 1) {
			window.location.href = ROOT + URL_MODULE + '/' + (URL_ACTION == 'register' || URL_ACTION == 'confirm' ? 'index' : URL_ACTION);
		} else {
			$('span.n-seconds').html(count_n_sec + (count_n_sec > 1 ? ' segundos' : ' segundo'));
			setTimeout(redirect_timer, 1000);
		}
		count_n_sec--;
	}	
	setTimeout(redirect_timer, 1000);
</script>