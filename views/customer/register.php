<ol class="breadcrumb">
	<li><a href="<?php echo URL::root();?>">Home</a></li>
	<li><a href="<?php echo H::link('customer','login');?>">Login</a></li>
	<li class="active">Registre-se</li>
</ol>
<h2 class="main-heading text-center">Registre-se</h2>
<div id="content-register" class="col-sm-offset-2 col-sm-8">
	<?php include('form-register.php');?>
</div>
<div class="clear"></div>