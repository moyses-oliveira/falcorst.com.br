<div class="alert alert-success alert-dismissable text-center">
	<h4 style="color: #222;text-align: center;">Parabéns.</h4>
	<p style="color: #222;text-align: center;">Em alguns segundos você receberá um e-mail para finalizar seu cadastro.</p>
	<br/>
	<form action="<?php echo URL::link('customer','confirm');?>" enctype="multipart/form-data" method="post" class="default-form update_target" id="form-register-confirm" target="#content-register">
	
		<label>Digite seu código de verificação: </label><br/>
		<input type="text" value="" name="confirm_pass" placeholder="CÓDIGO DE VERIFICAÇÃO DE E-MAIL" />
		<button type="submit"><i class="fa fa-check"></i>&nbsp; Confirmar</button>
	</form>
	<br/>
</div>