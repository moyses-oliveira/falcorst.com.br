<?php
if(!isset($errors)) $errors = array();
printf('
	<div class="warnings field">
		<div class="box box-solid box-danger center" %s>
			<div class="box-header">
				<h3 class="box-title">Atenção</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-danger btn-sm" data-widget="collapse" onclick="return false;"><i class="fa fa-minus"></i></button>
					<button class="btn btn-danger btn-sm" data-widget="remove" onclick="return false;"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
				%s
			</div>
		</div>
	</div>',
	(count($errors) ? '' : ' style="display: none;"'),
	(count($errors) ? sprintf('<ul><li>%s</li><ul>', implode('</li><li>', $errors)) : '')
);
		
?>
	
	