<?php
$a = $address;
if(!empty($a->address_vrc_complement))
		$a->address_vrc_complement = ' / '.$a->address_vrc_complement;
printf('	
	<div class="text-center address-data">
		<h3>Endereço de Entrega</h3>
		<p><strong>%s - %s - %s</strong></p>
		<p><strong>CEP</strong> %s</p>
		<p><strong>Bairro </strong> %s</p>
		<p><strong>Endereço </strong>%s <strong>Nº </strong>%s %s </p>
	</div>
	',
	$a->address_vrc_city, $a->address_vrc_zone, $a->address_vrc_country, $a->address_vrc_postcode, 
	$a->address_vrc_neighborhood, $a->address_vrc_address, $a->address_vrc_number, $a->address_vrc_complement
);