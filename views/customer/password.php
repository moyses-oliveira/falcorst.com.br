
<?php 
if(count($_POST) && !$errors):
echo '
	<h3>Parabéns '. substr(MY_NAME, 0, strpos(MY_NAME, ' ')) .'!</h3>
	<h4>Sua senha foi alterada com sucesso.</h4>
';
else:
	echo '<h3>Alteração de Senha</h3>';
	include('form-warnings.php');
	printf('<form action="%s" enctype="multipart/form-data" method="post" class="default-form form-horizontal" id="form-password">', URL::link('customer/password'));
		$id_prefix = 'field-login';
		foreach($fields as $k=>$f):
			$type = in_array('pass', $f->type) ? 'password' : 'text';
			$class = implode(' ', $f->type);
			echo '<div class="form-group">';
			printf('
					<label for="%1$s-%2$s" class="control-label col-sm-3">%3$s</label>
					<input id="%1$s-%2$s" type="%4$s" name="%2$s" class="%5$s control-form col-sm-9" value="%6$s" />
				', 
				$id_prefix, $k, $f->label, $type, $class, $f->value);
			echo '</div>';
		endforeach;
	echo '
		<div class="field">
			<button type="submit" class="btn btn-danger login col-sm-9 col-sm-offset-3">
				<i class="fa fa-save"></i> &nbsp;Alterar Senha
			</button>
		</div>
	</form>';
endif;