	<div class="alert alert-success alert-dismissable text-center">
		<h4 style="color: #222;text-align: center;">Conectando.</h4>
		<p style="color: #222;text-align: center;">Você será redirecionado em <span class="n-seconds">3 segundos</span>.</p>
		<br/>
	</div>
		
	<script type="text/javascript">
		var count_n_sec = 3;
		setInterval(function(){
			count_n_sec--;
			if(count_n_sec < 1) {
				window.location.href = '<?php echo URL::root() . 'customer';?>';
				return false;
			}
			$('span.n-seconds').html(count_n_sec + (count_n_sec > 1 ? ' segundos' : ' segundo'));
		}, 1000);
	</script>