<ol class="breadcrumb">
	<li><a href="<?php echo URL::root();?>">Home</a></li>
	<li class="active">Login</li>
</ol>

<h2 class="main-heading text-center">Login</h2>

<div class="panel panel-smart col-sm-6">

	<div class="panel-heading">
		<h3 class="panel-title">Já é cliente?</h3>
	</div>
	<div class="panel-body" id="content-login">
			<?php include('form-login.php');?>
	</div>
</div>

<div class="panel panel-smart col-sm-6">
	<div class="panel-heading">
		<h3 class="panel-title">Ainda não é cliente?</h3>
	</div>
	<div class="panel-body">
		<p>Clique no botão registre-se para se cadastrar.</p>
		<a class="btn btn-danger col-sm-offset-3 col-sm-6" href="<?php echo H::link('customer', 'register');?>" >
			<i class="fa fa-edit"></i> Registre-se
		</a>
	</div>
</div>
<div class="clear"></div>