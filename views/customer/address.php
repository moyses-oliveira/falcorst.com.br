<div style="min-height: 220px;">
	<h3>Endereços</h3>
	
	<div class="<?php isset($select) ? '' : 'hide-select';?> ">
	<?php 
		foreach($addresses as $a):
			echo '<div class="address-data">';
			$class=isset($select) ? 'col-md-8' : 'col-md-12';
			$btnSelect= '';
			if(isset($select)):
				$btnSelect= sprintf('
				<div class="col-md-4 text-right">
				<a href="%s" class="btn btn-success"><i class="fa fa-map-marker"></i> &nbsp;Usar este endereço</a>
				</div>', URL::link('purchase','step-3',$a->address_int_id));
			endif;
			if(!empty($a->address_vrc_complement))
					$a->address_vrc_complement = ' / '.$a->address_vrc_complement;
			printf('
				<div class="%s">
				<p><strong>%s - %s - %s</strong></p>
				<p><strong>CEP</strong> %s</p>
				<p><strong>Bairro</strong> %s </p>
				<p><strong>Endereço</strong> %s <strong>Nº</strong> %s %s </p>
				</div>
				%s
				<div class="clear"></div>
				',
			$class, $a->address_vrc_city, $a->address_vrc_zone, $a->address_vrc_country, 
			$a->address_vrc_postcode, $a->address_vrc_neighborhood, $a->address_vrc_address, 
			$a->address_vrc_number, $a->address_vrc_complement , $btnSelect
			);
			echo '</div>';
		endforeach;
	?>
		<div class="clear"></div>
	</div>
	<a href="#" class="btn btn-block btn-success text-center" onclick="$(this).hide();$('#new-address').animate({height: 'show'},1000);return false;">
		<i class="fa fa-map-marker"></i> &nbsp;Clique aqui para adicionar um novo endereço
	</a>
<?php
echo '<br/>';
include('form-warnings.php');
	
echo '
<div class="panel panel-smart" id="new-address" style="display: none">

		<div class="panel-heading">
			<h3 class="panel-title">Cadastro de endereço</h3>
		</div>
		<div class="panel-body">';
		
	printf('<form action="%s" enctype="multipart/form-data" method="post" class="default-form form-horizontal" id="form-new-address">', URL::link(H::module(),H::action()));
		$prefix = 'addr';
		$k = 'address_vrc_postcode'; $f = $fields[$k];
		echo '<div class="field form-group">';
		printf('<label for="%1$s-%2$s" class="col-sm-3 control-label">%3$s</label>', $prefix, $k, $f->label);
		printf('<input id="%1$s-%2$s" type="text" name="%1$s[%2$s]" class="cep col-sm-5 form-control" value="%3$s" />',
					$prefix, $k, $f->value);
		echo '<a href="#" class="btn btn-danger btn-busca-cep"><i class="fa fa-search"></i></a>';
		echo '</div>';
		
		$k = 'address_int_zone'; $f = $fields[$k];
		echo '<div class="field form-group">';
		$options = '';
		
		foreach($f->options as $o):
			$selected = $o->key == $f->value ? 'selected="selected"' : '';
			$options .= sprintf('
				<option value="%s" code="%s" %s>%s</option>' , $o->key, $o->code, $selected, $o->value);
		endforeach;
		
		printf('<label for="%1$s-%2$s" class="col-sm-3 control-label">%3$s</label>', $prefix, $k, $f->label);
		printf('
			<select id="%1$s-%2$s" type="text" name="%1$s[%2$s]" class="%3$s col-sm-9 form-control">%4$s</select>',
			$prefix, $k, 'not_null', $options
		);
		
		echo '</div>';
		
		foreach($fields as $k=>$f):
			if(in_array($k, array('address_int_zone', 'address_int_country', 'address_vrc_postcode')))
				continue;
			
			$class = implode(' ', $f->type);
			echo '<div class="field form-group">';
			printf('	
					<label for="%1$s-%2$s" class="col-sm-3 control-label">%3$s</label>',
					$prefix, $k, $f->label
				);
			if($k == 'address_vrc_address'):
				printf('
					<textarea id="%1$s-%2$s" type="hidden" name="%1$s[%2$s]" class="%3$s col-sm-9 form-control">%4$s</textarea>',
					$prefix, $k, $class, $f->value
				);
			else:
				printf('
					<input id="%1$s-%2$s" type="text" name="%1$s[%2$s]" class="%3$s col-sm-9 form-control" value="%4$s" />',
					$prefix, $k, $class, $f->value
				);
			endif;
			echo '</div>';
		endforeach;
		echo '
			
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
					<button type="submit" class="btn btn-danger btn-block">
						<i class="fa fa-edit"></i> &nbsp;Salvar Endereço
					</button>
				</div>
			</div>
		</form>
	
	</div>
</div>';
?>
</div>