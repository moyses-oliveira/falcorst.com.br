<div class="panel panel-smart">
	<?php 
	if(H::action() == 'register'):
		printf('
		<a href="%s" class="btn btn-default btn-block">
			<i class="fa fa-arrow-left"></i> &nbsp;Volgar a tela de login.
		</a>', H::link('customer','login'));
	endif;
	?>
	<div class="panel-body">

		<?php include('form-warnings.php');?>

		<!-- Registration Form Starts -->
		<form action="<?php echo URL::link('customer','register');?>" enctype="multipart/form-data" method="post" class="default-form form-horizontal update_target" id="form-register" target="#content-register" role="form">
			<?php 
			$id_prefix = 'field-reg';
			foreach($fieldsRegister as $k=>$f):
				$type = in_array('pass', $f->type) ? 'password' : 'text';
				$class = implode(' ', $f->type);
				printf('
					<div class="field form-group">
						<label for="%1$s-%2$s" class="col-sm-3 control-label">%3$s:</label>
						<input id="%1$s-%2$s" type="%4$s" name="%2$s" class="form-control col-sm-9 %5$s" value="%6$s" />
					</div>
					', 
					$id_prefix, $k, $f->label, $type, $class, $f->value);
			endforeach;
			?>
			
			<h3 class="panel-title">
				Termos e Condições de Compra e Venda de Produtos
			</h3>
			<div class="panel-body" style="max-height: 350px;overflow: auto;padding: 5px 20px;margin: 20px 0;">
				<?php echo $use_terms;?>
			</div>			
			<div class="field">
				<div class="text-center">
					<label style="font-size: 1.2em;margin: 15px 0;" class=" text-danger">
						<input type="checkbox" value="1" name="accept" class="acceptterms" />
						Li e aceito os <strong>Termos de uso</strong> dos serviços deste site!
					</label>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-offset-6 col-sm-6">
					<button type="submit" class="btn btn-danger btn-block">
						<i class="fa fa-edit"></i> &nbsp;Registrar
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="clear"></div>
<script>
$(document).ready(function(){
	validator.vTypes.push('acceptterms');
	validator.warnings.acceptterms = 'Você não pode cadastrar sem aceitar os <strong>Termos de uso</strong>.',
	validator.test.acceptterms = function(v){
		return $('input.acceptterms').first().is(":checked");
	}
	
	validator.vTypes.push('compare');
	validator.warnings.compare = 'O campo {0} não confere.',
	validator.test.compare = function(v){
		return $('#field-reg-vrc_pass').val() == v;
	}
});
</script>
