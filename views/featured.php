<!-- Featured Products Starts -->
<section class="products-list">			
<!-- Heading Starts -->
	<h2 class="product-head">Featured Products</h2>
<!-- Heading Ends -->
<!-- Products Row Starts -->
	<div class="row">
	<!-- Product #1 Starts -->
		<div class="col-md-3 col-sm-6">
			<div class="product-col">
				<div class="image">
					<img src="<?php echo H::root();?>files/images/product-images/17.jpg" alt="product" class="img-responsive" />
				</div>
				<div class="caption">
					<h4><a href="product.html">Grocery Items</a></h4>
					<div class="description">
						We are so lucky living in such a wonderful time. Our almost unlimited ...
					</div>
					<div class="price">
						<span class="price-new">$199.50</span> 
						<span class="price-old">$249.50</span>
					</div>
					<div class="cart-button button-group">
						<button type="button" class="btn btn-cart">
							<i class="fa fa-shopping-cart"></i>
							Add to cart									 
						</button>
						<button type="button" title="Wishlist" class="btn btn-wishlist">
							<i class="fa fa-heart"></i>
						</button>
						<button type="button" title="Compare" class="btn btn-compare">
							<i class="fa fa-bar-chart-o"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	<!-- Product #1 Ends -->
	<!-- Product #2 Starts -->
		<div class="col-md-3 col-sm-6">
			<div class="product-col">
				<div class="image">
					<img src="<?php echo H::root();?>files/images/product-images/18.jpg" alt="product" class="img-responsive" />
				</div>
				<div class="caption">
					<h4><a href="product.html">Grocery Items</a></h4>
					<div class="description">
						We are so lucky living in such a wonderful time. Our almost unlimited ...
					</div>
					<div class="price">
						<span class="price-new">$199.50</span> 
						<span class="price-old">$249.50</span>
					</div>
					<div class="cart-button button-group">
						<button type="button" class="btn btn-cart">
							<i class="fa fa-shopping-cart"></i>
							Add to cart									 
						</button>
						<button type="button" title="Wishlist" class="btn btn-wishlist">
							<i class="fa fa-heart"></i>
						</button>
						<button type="button" title="Compare" class="btn btn-compare">
							<i class="fa fa-bar-chart-o"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	<!-- Product #2 Ends -->
	<!-- Product #3 Starts -->
		<div class="col-md-3 col-sm-6">
			<div class="product-col">
				<div class="image">
					<img src="<?php echo H::root();?>files/images/product-images/19.jpg" alt="product" class="img-responsive" />
				</div>
				<div class="caption">
					<h4><a href="product.html">Grocery Items</a></h4>
					<div class="description">
						We are so lucky living in such a wonderful time. Our almost unlimited ...
					</div>
					<div class="price">
						<span class="price-new">$199.50</span> 
						<span class="price-old">$249.50</span>
					</div>
					<div class="cart-button button-group">
						<button type="button" class="btn btn-cart">
							<i class="fa fa-shopping-cart"></i>
							Add to cart									 
						</button>
						<button type="button" title="Wishlist" class="btn btn-wishlist">
							<i class="fa fa-heart"></i>
						</button>
						<button type="button" title="Compare" class="btn btn-compare">
							<i class="fa fa-bar-chart-o"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	<!-- Product #3 Ends -->
	<!-- Product #4 Starts -->
		<div class="col-md-3 col-sm-6">
			<div class="product-col">
				<div class="image">
					<img src="<?php echo H::root();?>files/images/product-images/20.jpg" alt="product" class="img-responsive" />
				</div>
				<div class="caption">
					<h4><a href="product.html">Grocery Items</a></h4>
					<div class="description">
						We are so lucky living in such a wonderful time. Our almost unlimited ...
					</div>
					<div class="price">
						<span class="price-new">$199.50</span> 
						<span class="price-old">$249.50</span>
					</div>
					<div class="cart-button button-group">
						<button type="button" class="btn btn-cart">
							<i class="fa fa-shopping-cart"></i>
							Add to cart									 
						</button>
						<button type="button" title="Wishlist" class="btn btn-wishlist">
							<i class="fa fa-heart"></i>
						</button>
						<button type="button" title="Compare" class="btn btn-compare">
							<i class="fa fa-bar-chart-o"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	<!-- Product #4 Ends -->
	</div>
<!-- Products Row Ends -->
</section>
<!-- Latest Products Ends -->