<?php
$filter_prefix = '?';
echo '<h3 class="side-heading">Filtros</h3>';
echo '<div class="list-group">';

	if($active_filters || isset($_GET['maker'])):
		echo '
		<div class="list-group-item">Filtros Ativos</div>
		<div class="list-group-item">
			<ul class="filter-group">
		';
		
		if(isset($_GET['maker'])):
			$httpQuery = $_GET;
			unset($httpQuery['maker']);
			printf('<li style="display: block;" class=""><a href="%1$s" class="text-danger"><i class="fa fa-times"></i> %2$s: %3$s</a></li>' . PHP_EOL, 
				'?'.http_build_query($httpQuery), 'Fabricante', $makers[0]->product_maker_vrc_name);		

			$makers = array();
		endif;
		foreach($active_filters as $filter):
		
			$httpQuery = $_GET;
			$httpQuery['f'] = isset($httpQuery['f']) ? json_decode(base64_decode($httpQuery['f'])) : array();
			$httpQuery['f'] = array_diff($httpQuery['f'], array($filter->psf_int_filter_item));
			sort($httpQuery['f']);
			$httpQuery['f'] = base64_encode(json_encode($httpQuery['f']));
			printf('<li style="display: block;" class=""><a href="%1$s" class="text-danger"><i class="fa fa-times"></i> %2$s: %3$s</a></li>' . PHP_EOL, 
				'?'.http_build_query($httpQuery), $filter->filter_label_vrc_label, $filter->filter_value_vrc_value);
		endforeach;
		echo '	
			</ul>
		</div>';
	endif;

	if($makers):
		echo '
		<div class="list-group-item">Fabricante</div>
		<div class="list-group-item">
			<ul class="filter-group">
		';
		foreach($makers as $maker):
			$httpQuery = $_GET;
			$httpQuery['maker'] = $maker->product_maker_int_id;
			printf('
				<li style="display: block;" class=""><a href="%1$s"><i class="fa fa-caret-right"></i> %2$s (%3$s)</a></li>' . PHP_EOL, 
			$filter_prefix . http_build_query($httpQuery), $maker->product_maker_vrc_name, $maker->total_int_product);
		endforeach;
		echo '	
			</ul>
		</div>';
	endif;
	$filter_label = null;
	if($filters):
		foreach($filters as $filter):
			if($filter_label != $filter->filter_label_vrc_label):
				if($filter_label != null):
					echo '	
					</ul>
				</div>';
				endif;
				$filter_label = $filter->filter_label_vrc_label;
				printf('
				<div class="list-group-item">%s</div>
				<div class="list-group-item">
					<ul class="filter-group">
					', $filter->filter_label_vrc_label);
			endif;
			$httpQuery = $_GET;
			$httpQuery['f'] = isset($httpQuery['f']) ? json_decode(base64_decode($httpQuery['f'])) : array();
			$httpQuery['f'][]=$filter->psf_int_filter_item;
			$httpQuery['f'] = base64_encode(json_encode($httpQuery['f']));
			printf('
						<li style="display: block;" class=""><a href="%1$s"><i class="fa fa-caret-right"></i> %2$s (%3$s)</a></li>' . PHP_EOL, 
				$filter_prefix . http_build_query($httpQuery), $filter->filter_value_vrc_value, $filter->total_int_product);
		endforeach;
		echo '	
			</ul>
		</div>';
	endif;
echo '</div>';