<div class="fullwidthbanner-container slider-revolution">
	<div class="fullwidthbanner">
		<ul>
			<li data-transition="fade">
			
				<img src="<?php echo H::root();?>files/content/revolution-slider/1920x610-2.jpg" alt="">

				<div class="tp-caption sfr background-blue"
					 data-x="20"
					 data-y="155"
					 data-speed="600"
					 data-start="1000"
					 data-easing="easeOutExpo">
				</div>
				
				<div class="tp-caption sfr background-blue-inner-1"
					 data-x="60"
					 data-y="200"
					 data-speed="600"
					 data-start="1500"
					 data-easing="easeOutExpo">
					 <h4>Competência + Comprometimento</h4>
				</div>
				
				<div class="tp-caption sfr background-blue-inner-2"
					 data-x="60"
					 data-y="250"
					 data-speed="600"
					 data-start="2000"
					 data-easing="easeOutExpo">
					 Buscando a excelência empresarial								 
				</div>
				
				<div class="tp-caption sfr background-blue-inner-1"
					 data-x="60"
					 data-y="280"
					 data-speed="600"
					 data-start="2500"
					 data-easing="easeOutExpo">
					 <i class="fa fa-heart"></i> &nbsp;102 Clientes atendidos com contratos anuais 
				</div>
				
				<div class="tp-caption lfl rounded background-light-blue hidden-phone hidden-tablet"
					 data-x="770"
					 data-y="95"
					 data-speed="1000"
					 data-start="3500"
					 data-easing="easeOutExpo">									 
					<h5>Head<br/>Hunter</h5> 									 
				</div>
				
				<div class="tp-caption lfl rounded background-dark-blue hidden-phone hidden-tablet"
					 data-x="550"
					 data-y="395"
					 data-speed="1000"
					 data-start="3000"
					 data-easing="easeOutExpo">							 
					<h5>Plano<br/>de<br/>Negócios</h5> 		 									 
				</div>
				
				<div class="tp-caption lfl rounded background-medium-blue hidden-phone hidden-tablet"
					 data-x="930"
					 data-y="215"
					 data-speed="1000"
					 data-start="4000"
					 data-easing="easeOutExpo">							 
					<h4>Coaching</h4>											 
				</div>
				
			</li>
			
			<li data-transition="fade">
			
				<img src="<?php echo H::root();?>files/content/revolution-slider/1920x610-1.jpg" alt="">

				<div class="tp-caption sfr background-blue"
					 data-x="20"
					 data-y="155"
					 data-speed="600"
					 data-start="1000"
					 data-easing="easeOutExpo">
				</div>
				
				<div class="tp-caption sfr background-blue-inner-1"
					 data-x="60"
					 data-y="200"
					 data-speed="600"
					 data-start="1500"
					 data-easing="easeOutExpo">
					 <h4>Competência + Comprometimento</h4>
				</div>
				
				<div class="tp-caption sfr background-blue-inner-2"
					 data-x="60"
					 data-y="250"
					 data-speed="600"
					 data-start="2000"
					 data-easing="easeOutExpo">
					 Buscando a excelência empresarial								 
				</div>
				
				<div class="tp-caption sfr background-blue-inner-1"
					 data-x="60"
					 data-y="280"
					 data-speed="600"
					 data-start="2500"
					 data-easing="easeOutExpo">
					 <i class="fa fa-star"></i> &nbsp;1.284 horas de eventos realizadas <br/>
					 <i class="fa fa-star"></i> &nbsp;14.680 horas realizadas pela equipe interna e por parceiros 
				</div>
				
				<div class="tp-caption lfl rounded background-light-blue hidden-phone hidden-tablet"
					 data-x="770"
					 data-y="95"
					 data-speed="1000"
					 data-start="3000"
					 data-easing="easeOutExpo">									 
					<h5>Palestras<br/>Empresariais</h5> 									 
				</div>
				
				<div class="tp-caption lfl rounded background-dark-blue hidden-phone hidden-tablet"
					 data-x="550"
					 data-y="395"
					 data-speed="1000"
					 data-start="3500"
					 data-easing="easeOutExpo">							 
					<h5>Metodologia<br/>exclusiva</h5> 		 									 
				</div>
				
				<div class="tp-caption lfl rounded background-medium-blue hidden-phone hidden-tablet"
					 data-x="930"
					 data-y="215"
					 data-speed="1000"
					 data-start="4000"
					 data-easing="easeOutExpo">							 
					<h4>Treinamentos<br/>In<br/>Company</h4>											 
				</div>
				
			</li>
			
		</ul>			
	</div><!-- end .fullwidthbanner -->
</div><!-- end .fullwidthbanner-container -->
