<div id="page-header">
				
	<div class="container">
		<div class="row">
			<div class="span12">
				<h1 style="color: #fff;"><?php echo $_title;?></h1>
			</div><!-- end .span12 -->
			
		</div><!-- end .row -->
	</div><!-- end .container -->
	
</div>
<div class="container">
	<div class="row">
		<div class="span12">
			<div class="portfolio-grid three-cols gutter portfolio-isotope popup-gallery">
			<?php
			$tpl = '
				<div class="item">
					<div class="portfolio-item">
						<div class="portfolio-item-preview">			
							<img src="%4$s" alt="%1$s" title="%2$s">
							<div class="portfolio-item-overlay">
								<div class="portfolio-item-description">
									<h4>
									<a href="%3$s" title="%2$s">%1$s</a>
									</h4>
								</div><!-- end .portfolio-item-description -->
							</div><!-- end .portfolio-item-overlay -->
						</div><!-- end .portfolio-item-preview -->
					</div><!-- end .portfolio-item -->
				</div><!-- end .item -->';
			$thumb_path = 'files/gallery/thumbs/';
			$img_path = 'files/gallery/images/';
			foreach($images as $p):
				$path = substr($p->img, 0,-1 * strlen(basename($p->img)));
				$thumb = H::root() . ImagePlugin::resize($img_path . $p->img, trim($thumb_path . $path,'/'), 600, 600, false);
				printf($tpl, $p->alt, $p->title,  H::root() . $img_path . $p->img,  $thumb);
			endforeach;
			?>
			</div><!-- end .portfolio-grid -->
		</div><!-- end .span12 -->
	</div><!-- end .row -->
</div>
<script type="text/javascript">
  $(document).ready(function() {
	$('.popup-gallery').magnificPopup({
	  delegate: 'a',
	  type: 'image',
	  tLoading: 'Loading image #%curr%...',
	  mainClass: 'mfp-img-mobile',
	  gallery: {
		enabled: true,
		navigateByImgClick: true,
		preload: [0,1] // Will preload 0 - before current, and 1 after the current image
	  },
	  image: {
		tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		titleSrc: function(item) {
		  return item.el.attr('title');
		}
	  }
	});
  });
</script>