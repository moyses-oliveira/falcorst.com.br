<!-- 3 Column Banners Starts -->
<div class="col3-banners">
	<ul class="row list-unstyled">
		<li class="col-sm-4">
			<img src="<?php echo H::root();?>files/images/banners/3col-banner1.jpg" alt="banners" class="img-responsive" />
		</li>
		<li class="col-sm-4">
			<img src="<?php echo H::root();?>files/images/banners/3col-banner2.jpg" alt="banners" class="img-responsive" />
		</li>
		<li class="col-sm-4">
			<img src="<?php echo H::root();?>files/images/banners/3col-banner3.jpg" alt="banners" class="img-responsive" />
		</li>
	</ul>
</div>
<!-- 3 Column Banners Ends -->