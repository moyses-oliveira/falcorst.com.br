<div class="product">
<!-- Breadcrumb Starts -->
	<ol class="breadcrumb">
		<?php
			printf('<li><a href="%s">Home</a></li>', H::root());
			printf('<li><a href="%s">%s</a></li>', H::link('catalog'), 'Produtos');
			$catURL = H::link('catalog',URL::build($data->category, $data->product_int_category));
			printf('<li><a href="%s">%s</a></li>', $catURL, $data->category);
			printf('<li class="active">%s</li>', $desc->product_description_vrc_name);
		?>
	</ol>
<!-- Breadcrumb Ends -->
<!-- Product Info Starts -->
	<div class="row product-info">
	<!-- Left Starts -->
		<div class="col-sm-6 images-block">
			<?php
			
			$no_img = H::root() . 'files/images/product-images/pimg3.jpg';
			$img = count($img_data_list) > 0 ? current($img_data_list) : null;
			$tagImg = tag::img($no_img,'');
			if(!!$img):
				$path = substr($img->img, 0,-1 * strlen(basename($img->img)));
				$resize = H::root() . ImagePlugin::resize(PATH_IMAGES . $img->img, trim(PATH_THUMBS . $path,'/'), 900, 900, false);
				$tagImg = tag::img($resize, $img->title, $img->alt);
			endif;
			printf('
			<p><span class="img-zoom">%s</span></p>' . PHP_EOL,  $tagImg);
			if(count($img_data_list) > 1):
				echo '
				<ul class="list-unstyled list-inline">';
				foreach($img_data_list as $img):
					$path = substr($img->img, 0,-1 * strlen(basename($img->img)));
					$resize = H::root() . ImagePlugin::resize(PATH_IMAGES . $img->img, trim(PATH_THUMBS . $path,'/'), 900, 900, false);
					$thumb  = H::root() . ImagePlugin::resize(PATH_IMAGES . $img->img, trim(PATH_THUMBS . $path,'/'), 77, 77, true);
					$tagThumb = tag::img($thumb, $img->title, $img->alt);
					printf('
					<li>%s</li>', tag::a($resize, $tagThumb, $img->title));
				endforeach;
				echo '
				</ul>';
			endif;
			
			?>
		</div>
	<!-- Left Ends -->
	<!-- Right Starts -->
		<div class="col-sm-6 product-details">
		<!-- Product Name Starts -->
			<h2><?php echo $desc->product_description_vrc_name;?></h2>
			<div id="share-product" class="text-right"></div>
			<div class="price">
				<span class="price-head">R$</span>
				<span class="price-new"><?php echo number_format($data_price->price, 2, ',', '.');?></span> 
			</div>
		<!-- Product Name Ends -->
			<hr>
		<!-- Manufacturer Starts -->
			</p>
				<?php echo $desc->product_description_vrc_summary;?>
			</p>
		<!-- Manufacturer Ends -->
			
		<!-- Price Starts -->
				<?php
				if(count($subproducts) > 1):
					echo '<hr/>';
					echo '<div class="form-horizontal">';
					echo '<div class="form-group option">';
					$v = H::link('product', URL::build($desc->product_description_vrc_name, $sid));
					$attrField = array('onchange'=>'$(this).val() && (window.location = $(this).val());');
					$attrLabel = array('class'=>'control-label col-sm-4');
					echo (new form())->select(array('product_option'), 'Opção de Produto', $v, $select_subproducts, 'form-control col-sm-8', $attrField, $attrLabel);
					echo '</div>';
					echo '</div>';
				endif;
				if(count($options_groups)):
					echo '<hr/>';
					echo '<div class="form-horizontal">';
					foreach($options_groups as $k=>$g):
						printf('
						<div class="form-group option col-sm-12">
							<label class="control-label col-sm-4" for="select-spopt%s">%s</label>
							<select class="form-control col-sm-8" name="options[]" id="select-spopt%s">',
							$k, $g->label, $k
						);
						foreach($g->options as $o):
							printf('<option value="%s">%s</option>', $o[0], $o[1]);
						endforeach;
						echo '
							</select>
						</div>';
						
					endforeach;
					echo '</div>';
				endif;
				?>
				<div class="clear"></div>
				<hr/>
				<div class="form-group qtt col-sm-6">
					<label class="control-label" for="input-quantity">Qtd:</label>
					<a href="#" class="qtt-action less disabled" value="-1"><i class="fa fa-minus-square"></i></a>
					<input type="text" name="qtt" id="field-qtt" value="1" readonly="readonly" class="form-control"/>
					<a href="#" class="qtt-action" value="+1"><i class="fa fa-plus-square"></i></a>
				</div>
				<div class="col-sm-6">
					<button type="button" class="btn btn-cart" url="<?php echo URL::link('cart','add', $sid);?>">
						<i class="fa fa-shopping-cart"></i>Adicionar ao carrinho
					</button>
				</div>
				<div class="clear"></div>
				<div class="">
					<form id="form-shipping" action="<?php echo H::link('product', 'get-shipping.json');?>" class="form-inline text-center" method="GET">
							<div class="form-group">
							<input type="hidden" name="product" value="<?php echo $sid;?>" />
							<label class="control-label">Cálcular Frete: </label>
							<input type="text" name="cep" placeholder="CEP" style="width: 110px;" class="form-control">
							<button class="btn btn-primary btn-small" type="submit"><i class="fa fa-truck"></i></button>
							</div>
					</form>
				</div>
				<div class="clear"></div>
			</div>
		<!-- Price Ends -->
		<!-- Available Options Starts -->
			<div class="calc-shipping-content  col-sm-offset-1  col-sm-10 "></div>
				<div class="clear"></div>
		<!-- Available Options Ends -->
			<hr>
		</div>
	<!-- Right Ends -->
	</div>
<!-- product Info Ends -->
<!-- Product Description Starts -->
	<div class="product-info-box">
		<h4 class="heading">Descrição</h4>
		<div class="content panel-smart">
			<?php echo $desc->product_description_txt_description;?>
		</div>
	</div>
<!-- Product Description Ends -->
<!-- Additional Information Starts -->
	<div class="product-info-box" id="attributes">
		<h4 class="heading">Informações Técnicas</h4>
		<?php
		/* TECHNICAL INFORMATION OPEN */ 
		$attr_tpl = '
			<div class="attr">
				<label>%s:</label>
				<span>%s</span>
			</div>';

		if(!empty($data->maker))
			echo PHP_EOL . sprintf($attr_tpl, 'Fabricante', $data->maker);
		
		#echo PHP_EOL . sprintf($attr_tpl, 'Largura', $sub->product_sub_dcm_width * 100 . ' cm' );
		#echo PHP_EOL . sprintf($attr_tpl, 'Altura', $sub->product_sub_dcm_height * 100 . ' cm');
		#echo PHP_EOL . sprintf($attr_tpl, 'Comprimento', $sub->product_sub_dcm_length * 100 . ' cm');
		
		foreach($attr_data_list as $at):
			if($at->product_attribute_chr_language != 'pt_br') continue;
			echo PHP_EOL . sprintf($attr_tpl, $at->product_attribute_vrc_label, $at->product_attribute_txt_description );
		endforeach;
		/* TECHNICAL INFORMATION CLOSE */

		?>
	</div>
<!-- Additional Information Ends -->