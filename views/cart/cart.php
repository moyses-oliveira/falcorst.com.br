
<div class="table-responsive shopping-cart-table">
<?php
if(!$shopping_cart_data->items):
	echo H::msgBox('<h4 class="text-center"> O carrinho está vazio </h4>', true, H::WARNING);
else:
	echo '
		<table class="table table-bordered">
			<thead>
				<tr>
				';

	echo '<table class="table table-bordered table-shopping-cart">';
	echo '
	<tr>
		<th>Produto</th>
		<th style="width: 15%">Preço Unit.</th>
		<th style="width: 20%">Qtd.</th>
		<th style="width: 15%">$</th>
		'.
		(!isset($hide_btn_purchase) ? '<th style="min-width: 50px">&nbsp;</th>' : '')
		.'
	</tr>
	';
	$action_column_tpl = '<td class="text-center">
					<a href="%s" class="remove-from-cart"><i class="fa fa-close"></i></a>
				</td>';
	$cart_item_tpl = '
			<tr class="{has_stock}">
				<td class="product-name">
					<a class="image-modal-viewer" href="{img_url}" title="{name}"><i class="fa fa-image"></i></a>
					&nbsp; {full_name}
				</td>
				<td>R$ {price}</td>
				<td>{quantity}</td>
				<td>R$ {subtotal}</td>
				{action_column}
				</tr>
		';
	$total = 0;
	foreach($shopping_cart_data->items as $i):			
		$i->img_url = H::root() . PATH_IMAGES . $i->img;
		$i->action_column = sprintf($action_column_tpl,  H::link('cart','remove', $i->cart_item_bgn_id));
		if(isset($hide_btn_purchase))
			$i->action_column = '';
		
		$i->full_name = $i->name . (!$i->code ? '' : ' ['. $i->code .']') . ' ' . $i->resume_options;
		$i->has_stock = !$i->short ? '' : 'no-stock';
		echo TPL::format($cart_item_tpl, $i);
	endforeach;
	
	printf('
		<tr class="warning">
			<td colspan="3">Total</td><td colspan="2">R$ %s</td>
		</tr>',
		$shopping_cart_data->total
	);
	echo '</table>';
	
	if(H::module() != 'purchase'):
		printf('<a href="%s" class="btn btn-primary pull-right"><i class="fa fa-money"></i> &nbsp;Fechar compra</a>', H::link('purchase','step-1'));
	endif;
	echo '<div class="clear"></div><hr/>';
	if(URL::friend(1) == 'cart'):
		printf('
			<div class="col-sm-12" style="border: 1px solid #aaa;">
				<h3 class="col-sm-6">Calcular Frete</h3>
				<form id="form-shipping" action="%s" class="form-inline col-sm-6" method="GET" style="padding-top: 15px;">
					&nbsp;<span>CEP: </span>
					<input type="text" name="cep" placeholder="CEP" style="width: 150px;display: inline-block;" class="form-control" />		
					<button class="btn btn-primary btn-small" type="submit" ><i class="fa fa-truck"></i></button>
				</form>
				
				<div class="calc-shipping-content col-sm-12"></div>
				<div class="clear"></div><br/>
			</div>',
			H::link('customer','get_shipping')
		);
	endif;
	echo '<div class="clear"></div><br/>';
endif;
?>

</div>