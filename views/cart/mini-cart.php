<?php
if(isset($GLOBALS['msgSettings'])):
	printf('<input type="hidden" class="preview-setting-erros" value="%s" />', base64_encode($GLOBALS["msgSettings"]));
	echo '
	<script type="text/javascript">
		$(document).ready(function(){
			var msg = decodeURIComponent(escape(atob($(".preview-setting-erros").val())));
			$("body").prepend(msg);
		});
	</script>
	';
endif;
?>
<div class="header-mini-cart">
	<?php 
	$mini_cart_items = Cart::getItems();
	printf('<input type="hidden" name="mini-cart-ajax-url" id="mini-cart-ajax-url" value="%s" />', URL::link('cart', 'header'));
	if(defined('MY_NAME')):
		$myname = explode(' ', MY_NAME);
		printf('
			<h3><a href="%s"><i class="fa fa-user" ></i> Olá %s %s</a></h3>
			<a href="#" class="h4 btn-show-cart"><i class="fa fa-shopping-cart"></i> &nbsp; Carrinho</a>
			<p>%s items adicionados.</p>', 
				URL::root() . 'customer/index',
				current($myname), 
				count($myname) == 1 ? '' : end($myname),
				count($mini_cart_items)
		);
	else:
		$btnLogin = QUOTE_MODE ? '' : sprintf('<p><a href="%s">Login / Registre-se</a></p>', URL::root() . 'customer/index');
		printf('
			<h3><i class="fa fa-user" ></i> Olá visitante</h3>
			<a href="#" class="h4 btn-show-cart"><i class="fa fa-shopping-cart"></i> &nbsp; Carrinho</a>
			%s
			<p>%s items adicionados.</p>',
			$btnLogin,
			count($mini_cart_items)
		);
	endif;
	?>
	
	<div class="mini-cart" style="display: none;">
		<div class="mini-cart-content" >
		<a href="" class="close"><i class="fa fa-close"></i></a>
		<div style="clear: both;"></div>
		<?php
		if(count($mini_cart_items)):
			echo '<table>';
			$total=0;
			foreach($mini_cart_items as $i):
				$subtotal = $i->price * $i->quantity;
				printf(
					'<tr class="%s"><td class="product">%s <span class="unit-price">R$ %s</span> <span class="qtd">Qtd. %s</span></td><td>R$ %s</td></tr>', 
					!$i->short ? '' : 'no-stock',
					$i->name . (!$i->code ? '' : ' ['. $i->code .']'),
					number_format($i->price,2,',','.'),
					number_format($i->quantity,0,',','.'),
					number_format($subtotal,2,',','.'),
					URL::root() . 'customer/remove-from-cart/' . $i->product_sub_int_id
				);
				$total += $subtotal;
			endforeach;
			printf('<tr class="total"><td>Total</td><td>R$ %s</td></tr>',number_format($total,2,',','.'));
			echo '</table>';
			if(!QUOTE_MODE):
				printf('<a href="%s" class="btn pull-right btn-success">Fechar compra &nbsp; <i class="fa fa-angle-double-right"></i></a>', URL::root() . 'customer/purchase-step-1');
			endif;
			printf('<a href="%s" class="btn pull-right btn-primary" style="margin-right: 5px"><i class="fa fa-shopping-cart"></i> &nbsp; %s</a>', 
				URL::root() . 'customer/cart', QUOTE_MODE ? 'Ver Carrinho' : 'Editar compra');
				
		else:
			echo '<p>' . count($mini_cart_items) . ' items adicionados.</p>';
		endif;
		?>
		<div style="clear: both;"></div>
		</div>
	</div>
</div>