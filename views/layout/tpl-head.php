<?php
echo  PHP_EOL .'<head>';
printf(PHP_EOL .'<title>%s</title>', TITLE . (!$_title ? '' : ' - ' . $_title));

foreach(H::meta() as $meta):
	echo PHP_EOL . '' . tag::meta($meta);
endforeach;

printf(PHP_EOL . '<link rel="shortcut icon" href="%sfiles/assets/favicon.png" />', URL::root());
foreach(H::css() as $css):
	if(substr($css, 0, 7) == 'http://' || substr($css, 0, 8) == 'https://'):
		printf(PHP_EOL . '<link rel="stylesheet" href="%s"  type="text/css" />', $css);
	else:
		printf(PHP_EOL . '<link rel="stylesheet" href="%sfiles/%s"  type="text/css" />', URL::root(), $css);
	endif;
endforeach;
foreach(H::js() as $js):
	printf(PHP_EOL . '<script type="text/javascript" src="%sfiles/js/%s"></script>', URL::root(), $js);
endforeach;
echo PHP_EOL . '</head>' . PHP_EOL;