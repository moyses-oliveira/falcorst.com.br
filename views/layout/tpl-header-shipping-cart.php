<button type="button" data-toggle="dropdown" class="btn btn-block btn-lg dropdown-toggle">
	<i class="fa fa-shopping-cart"></i>
	<span id="cart-total"><?php printf('%s item(s) - R$ %s', $shopping_cart_data->length, $shopping_cart_data->total)?></span>
	<i class="fa fa-caret-down"></i>
</button>
<ul class="dropdown-menu pull-right">
	<li>
		<table class="table cart">
			<?php 
			$rowTpl = '
				<tr>
					<td class="text-center" >
						<a href="{url}">
							<img src="{thumb}" alt="{name}" title="{name}" class="prod-thumb" />
						</a>
					</td>
					<td class="text-left">
						<div><a href="{url}">{name}</a></div>
						<div>
							R$ {price} &nbsp;|&nbsp; x {quantity} 
							<a href="#" class="remove-from-mini-cart pull-right"><i class="fa fa-times"></i></a>
						</div>
					</td>
				</tr>';
			foreach($shopping_cart_data->items as $i):
				echo TPL::format($rowTpl, $i);
				unset($i);
			endforeach;
			?>
		</table>
	</li>
	<li>
		<p class="text-right btn-block1">
			<?php echo tag::a(H::link('cart','index'), 'Visualizar compra', '');?>
		</p>
	</li>									
</ul>