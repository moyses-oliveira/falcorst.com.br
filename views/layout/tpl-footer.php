<footer id="footer-area">
<!-- Footer Links Starts -->
	<div class="footer-links row">
	<!-- Information Links Starts -->
		<div class="col-sm-4">
			<h5>Área do cliente</h5>
			<ul>
				<?php
				$tpl = '
				<li><a href="%s"><i class="fa fa-%s"></i> &nbsp;%s</a></li>';
				printf($tpl, H::root(), 'home', 'Home');
				if(defined('MY_NAME')):
					printf($tpl, H::link('customer', 'index'), 'user', MY_NAME);
					printf($tpl, H::link('customer', 'logout'), 'sign-out', 'Logout');
				else:
					printf($tpl, H::link('customer', 'index'), 'sign-in', 'Login');
					printf($tpl, H::link('customer', 'register'), 'edit', 'Registre-se');
				endif;
				printf($tpl, H::link('cart', 'index'), 'shopping-cart', 'Carrinho');
				
				?>
			</ul>
		</div>
	<!-- Information Links Ends -->
	<!-- Contact Us Starts -->
		<div class="col-sm-4 last">
			<h5>Contato</h5>
			<ul>
				<li>Falcor Store</li>
				<li>
					Email: <a href="#">contato@falcorst.com.br</a>
				</li>								
			</ul>
			<!--h4 class="lead">
				Tel: <span>1(234) 567-9842</span>
			</h4-->
			
			<!-- Follow Us Links Starts -->
				<div id="footer-share">
				</div>
			<!-- Follow Us Links Ends -->
		</div>
	<!-- Contact Us Ends -->
	<!-- Logo Starts -->
		<div class="col-sm-4 last">
			<h5>&nbsp;</h5>
			<br/>
			<img src="<?php echo H::root();?>files/images/logo.jpg" alt="Logo Falcor Store" title="Logo Falcor Store" class="img-responsive" />
			<br/>
			<h4 class="text-center">Made by <a href="http://55digital.com.br" title="55 Digital">55 Digital</a></h4>
		</div>
	<!-- Logo Ends -->
	</div>
<!-- Footer Links Ends -->
</footer>