<header id="header-area">
<!-- Nested Row Starts -->
	<div class="row">
	<!-- Logo Starts -->
		<div class="col-md-4 col-xs-12">
			<div id="logo">
				<a href="<?php echo H::root();?>"><img src="<?php echo H::root();?>files/images/logo.jpg" title="Grocery Shoppe" alt="Grocery Shoppe" class="img-responsive" /></a>
			</div>
		</div>
	<!-- Logo Ends -->
	<!-- Header Right Starts -->
		<div class="col-md-8 col-xs-12">
			<div class="row header-top">
			<!-- Header Links Starts -->
				<div class="col-xs-12">
					<div class="header-links">
						<ul class="list-unstyled list-inline pull-right text-righr">
							<?php
							$tpl = '<li><a href="%s"><i class="fa fa-%s"></i> &nbsp;%s</a></li>';
							printf($tpl, H::root(), 'home', 'Home');
							if(defined('MY_NAME')):
								printf($tpl, H::link('customer', 'index'), 'user', MY_NAME);
								printf($tpl, H::link('customer', 'logout'), 'sign-out', 'Logout');
							else:
								printf($tpl, H::link('customer', 'index'), 'sign-in', 'Login');
								printf($tpl, H::link('customer', 'register'), 'edit', 'Registre-se');
							endif;
							printf($tpl, H::link('cart', 'index'), 'shopping-cart', 'Carrinho');
							
							?>
						</ul>
					</div>
				</div>
			<!-- Header Links Ends -->
			<!-- Currency & Languages Starts -->
			<?php /*  
				<div class="col-md-3 col-xs-12">
					<div class="pull-right">							
					<!-- Languages Starts -->
						<div class="btn-group">
							<button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
								ENG
								<i class="fa fa-caret-down"></i>
							</button>
							<ul class="pull-right dropdown-menu">
								<li>
									<a tabindex="-1" href="#">English</a>
								</li>
								<li>
									<a tabindex="-1" href="#">French</a>
								</li>
							</ul>
						</div>
					<!-- Languages Ends -->
					<!-- Currency Starts -->
						<div class="btn-group">
							<button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
								$
								<i class="fa fa-caret-down"></i>
							</button>
							<ul class="pull-right dropdown-menu">
								<li><a tabindex="-1" href="#">Pound </a></li>
								<li><a tabindex="-1" href="#">US Dollar</a></li>
								<li><a tabindex="-1" href="#">Euro</a></li>
							</ul>
						</div>
					<!-- Currency Ends -->
					</div>
				</div>
			// */ ?>
			<!-- Currency & Languages Ends -->
		</div>
		<div class="row">
			<!-- Search Starts -->					
				<div class="col-md-7 col-xs-12">
					<div id="search">
						
						<form method="GET" action="<?php echo H::link('catalog');?>">
							<div class="input-group">
								<?php
								foreach($_GET as $k=>$v):
									if($k == 'search') continue;
									printf('
									<input type="hidden" name="%s" value="%s" />', $k, $v); 
								endforeach;
								?>
								<input type="text" class="form-control input-lg" placeholder="Search" name="search" value="<?php echo $header_search_input;?>" />
								<span class="input-group-btn">
									<button class="btn btn-lg" type="submit">
										<i class="fa fa-search"></i>
									</button>
								</span>
							</div>
						</form>
					</div>	
				</div>
			<!-- Search Ends -->						
			<!-- Shopping Cart Starts -->					
				<div class="col-md-5 col-xs-12">
					<div id="cart" class="btn-group btn-block">
					<?php include('tpl-header-shipping-cart.php'); ?>
					</div>
				</div>
			<!-- Shopping Cart Ends -->						
			</div>
		</div>
	<!-- Header Right Ends -->				
	</div>
<!-- Nested Row Ends -->
</header>