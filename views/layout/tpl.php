<!DOCTYPE html>
<html>
<?php include('tpl-head.php');?>

<body>
<!-- Wrapper Starts -->
<div id="wrapper" class="container">
	
	<!-- Header Section Starts -->
	<?php include('tpl-header.php');?>
	<!-- Header Section Ends -->
	
	<!-- Main Menu Starts -->
	<?php include('tpl-menu.php');?>
	<!-- Main Menu Ends -->
	
	<?php 
		if(defined('MY_ID') && !!@$_show_customer_menu):
			echo '
			<div class="col-sm-3 customer-menu">
				<ul>
				';
			$tpl = PHP_EOL . "\t\t\t". '<li><a href="%s"><span><i class="fa fa-%s"></i></span>%s</a></li>';
				printf($tpl, URL::link('customer','index'), 'home', 'Inicio');
				printf($tpl, URL::link('cart','index'), 'shopping-cart', 'Carrinho');
				printf($tpl, URL::link('purchase','all'), 'archive', 'Minhas Compras');
				printf($tpl, URL::link('customer','password'), 'key', 'Alterar Senha');
				printf($tpl, URL::link('customer','address'), 'map-marker', 'Endereços');
				printf($tpl, URL::link('customer','logout'), 'sign-out', 'Log Out');
			echo '
					</ul>
				</div>
				<div class="col-sm-9">
					<div class="customer-content">';
				include('views/' . $_content_file); 
			echo'
					</div>
				</div>
				<div class="clear"></div><br/>';
		else:
			include('views/' . $_content_file); 
		endif;
	?>

	<!-- Footer Section Starts -->
	<?php include('tpl-footer.php');?>
</div>
<!--
<div id="popup" class="modal-box">
  <header> <a href="#" class="js-modal-close close">×</a>
    <h4>&nbsp;</h4>
  </header>
  <div class="modal-body">&nbsp;</div>
  <!--footer> <a href="#" class="btn btn-small js-modal-close">Close</a> &nbsp;</footer- ->
</div>
-->
</body>
</html>