<nav id="main-menu" class="navbar" role="navigation">
<!-- Nav Header Starts -->
	<div class="navbar-header">
		<button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-cat-collapse">
			<span class="sr-only">Toggle Navigation</span>
			<i class="fa fa-bars"></i>
		</button>
	</div>
<!-- Nav Header Ends -->
<!-- Navbar Cat collapse Starts -->
	<div class="collapse navbar-collapse navbar-cat-collapse">
		<ul class="nav navbar-nav">
		<?php
		foreach($catTree as $cat):
			if($cat->children):
				$tpl = '<li><a  tabindex="-1" href="%1$s" title="%2$s">%2$s</a></li>';
				printf('
				<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10" onclick="return false;">
							%s
						</a>
					<ul class="dropdown-menu" role="menu">
				', $cat->title);
						
				foreach($cat->children as $child):
					$url = H::link('catalog', URL::build($child->title, $child->id));
					printf($tpl, $url, $child->title);
				endforeach;
				echo '
					</ul>
				</li>';
			else:
				$tpl = '<li><a href="%1$s" title="%2$s">%2$s</a></li>';
				$cat->url = H::link('catalog', URL::build($cat->title, $cat->id));
				printf($tpl, $cat->url, $cat->title);
			endif;
		endforeach;
		?>

		</ul>
	</div>
<!-- Navbar Cat collapse Ends -->
</nav>