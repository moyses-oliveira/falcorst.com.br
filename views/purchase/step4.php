<?php
include('step-progress.php');

echo '<h3 class="text-center">Passo 4 - Forma de Pagamento</h3>';
echo '<div class="col-md-6">';
include('purchase-items-table.php');
echo '</div>';
echo '<div class="col-xs-6">';
include('views/customer/single_address.php');
echo '</div>';
echo '<div class="clear"></div>';

echo '<div>';
	echo '<div style="border: 1px solid #ccc;padding: 10px;">';
	echo '<h4 class="text-center">Clique na forma de pagamento desejada</h4>';
	foreach($methods as $m):
		if($m->method == 'pay_on_delivery'):
			printf('
				<div class="col-xs-2 h2"><a href="%1$s" class="btn btn-success" style="white-space: normal; min-height: 100px;font-size: 18px;"><i style="font-size: 30px;" class="fa fa-money"></i><br/>Pagar no local</a></div>',
				$m->url
			);
		elseif($m->method == 'deposit'):
			printf('
				<div class="col-xs-2 h2"><a href="%1$s" class="btn btn-success" style="white-space: normal; min-height: 100px;font-size: 18px;"><i style="font-size: 30px;" class="fa fa-credit-card"></i><br/>Depósito ou Transferência</a></div>',
				$m->url
			);
		else:
			printf('
				<a href="%1$s" title="%2$s" class="col-xs-2">
					<img src="%3$s" alt="%2$s" title="%2$s" style="width: 100%%;">
				</a>
				',
				$m->url, 
				$m->method,
				sprintf('%sfiles/attachments/%s.png', H::root(), $m->method)
			);
		endif;
	endforeach;
	echo '<div class="clear"></div></div>';
echo '</div>';