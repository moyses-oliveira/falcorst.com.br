﻿<?php
include('step-progress.php');
$step_title = '<h3 class="text-center">Passo 2 - Endereço de entrega</h3>';
if(!!$errors):
	foreach($errors as $err):
		echo H::msgBox($err, false, H::DANGER);
	endforeach;
elseif(!$shopping_cart_data->items):
	echo '
		' . $step_title . '
		<div class="table-container"><h4 class="text-center"> O carrinho está vazio </h4></div>';
else:
	include('views/cart/cart.php');
	include('views/customer/address.php');
endif;
echo '<div class="clear"></div>';