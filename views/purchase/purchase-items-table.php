<?php
echo '<table class="table table-bordered table-shopping-cart">';
echo '
<tr>
	<th>Produto</th>
	<th style="width: 20%">Preço Unit.</th>
	<th style="width: 20%">Qtd.</th>
	<th style="width: 20%">Subtotal</th>
</tr>
';
$action_column = '<td class="text-center"><a href="%s" class="remove-from-cart"><i class="fa fa-close"></i></a></td>';
$item_tpl = '
		<tr>
			<td class="product-name">%s</td>
			<td>%s %s</td>
			<td>%s</td>
			<td>%s %s</td></tr>
	';
foreach($items as $i):
	printf($item_tpl, 
		$i->name . (!$i->code ? '' : ' ['. $i->code .']') . ' ' . $i->options ,
		$purchase->purchase_vrc_currency_simble,
		number_format($i->price,2,',','.'),
		number_format($i->quantity,0,',','.'),
		$purchase->purchase_vrc_currency_simble,
		number_format($i->subtotal,2,',','.')
	);
endforeach;

	printf('
		<tr class="info">
			<td colspan="3">Sub-total</td><td>%s %s</td>
		</tr>',
		$purchase->purchase_vrc_currency_simble,
		number_format($purchase->purchase_dcm_subtotal,2,',','.')
	);

	printf('
		<tr class="info">
			<td colspan="3">Modo de Envio (%s)</td><td>%s %s</td>
		</tr>',
		$purchase->purchase_vrc_shipping_method,
		$purchase->purchase_vrc_currency_simble,
		number_format($purchase->purchase_dcm_shipping_price,2,',','.')
	);

	printf('
		<tr class="warning">
			<td colspan="3">Total</td><td>%s %s</td>
		</tr>',
		$purchase->purchase_vrc_currency_simble,
		number_format($purchase->purchase_dcm_total, 2, ',', '.')
	);
echo '</table>';