<?php
include('step-progress.php');

echo '<h3 class="text-center">Passo 5 - Fim</h3>';
echo '<div class="col-md-6">';
include('purchase-items-table.php');
echo '</div>';


echo '<div class="col-xs-6">';

if($purchase->purchase_vrc_method == 'deposit')
	echo sprintf('<div><h3>Dados para depósito</h3><pre>%s</pre><br/></div>',$deposit_acc);

include('views/customer/single_address.php');
echo '</div>';
echo '<div class="clear"></div>';

echo '<div>';
	echo '<a href="' . URL::site() . '" class="btn btn-large btn-success btn-block">
		<span class="h2"><i class="fa fa-home"></i> &nbsp; Voltar ao Site</span>
	</a>';
echo '</div>';