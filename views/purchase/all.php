<?php 
$grid = new gridView();
$fields = array(
	'dtt_open' => 'Data', 
	'status' =>  'Situação da Venda',
	'total' => 'Total'
);

foreach($fields as $k=>$label)
  $grid->addColumn(new gridViewColumn($k, $label));
  
$url = URL::root() . 'purchase/details/{purchase_int_id}';
$grid->addActionColumn(H::module(),'purchase_int_id', '90px',array(
	array('action' => 'purchase', 'class' => 'fa-search-plus', 'title' => 'Visualizar', 'url'=>$url)
	),
	false
);

if(!!$data):
	foreach($data as &$i)
		$i->dtt_open = CData::format('d/m/Y H:i', $i->dtt_open);
	

	echo $grid->renderTable($data);
	
	echo '<div class="row">';
	printf('<div class="col-md-6">
		<ul class="pagination">
			%s
			%s
			%s
			%s
		</ul>
	</div>',
		$pag->linkPrev(), 
		$pag->getCurrentPages(), 
		$pag->getNavigationGroupLinks(), 
		$pag->linkNext()
	);
	
	echo '</div>';
else:
	echo '<br/><br/>'.H::msgBox('Nenhum item encontrado',true, H::WARNING);
endif;