<?php
include('step-progress.php');
?>
<div class="panel panel-smart col-sm-6">

	<div class="panel-heading">
		<h3 class="panel-title">Ainda não é cliente? <span>Registre-se<span></h3>
	</div>
	<div class="panel-body" id="content-register">
	<?php include('views/customer/form-register.php');?>

	</div>
</div>
<div class="panel panel-smart col-sm-6">

	<div class="panel-heading">
		<h3 class="panel-title">Já é cliente?</h3>
	</div>
	<div class="panel-body" id="content-login">
	<?php include('views/customer/form-login.php');?>

	</div>
</div><div class="clear"></div>
