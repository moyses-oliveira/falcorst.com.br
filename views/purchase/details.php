<?php


printf('<h3 class="text-center">Compra Nº %s</h3>', str_pad(URL::friend(2), 10, 0, STR_PAD_LEFT));
printf('<h4 class="text-center">%s</h4>', $purchase->purchase_vrc_status);

$status = $purchase->purchase_status_vrc_alias;

if($status == 'shipped'):
	$chStatusUrl = URL::link(H::module(), 'received', URL::friend(2)) . '?hash=' . urlencode($purchase->purchase_vrc_hash);
	echo H::msgBox(sprintf('<a href="%s" class="h4">Se você já recebeu a encomenda clique aqui.</a>', $chStatusUrl), false);
endif;


if($purchase->purchase_vrc_method == 'deposit' && !$deposit_acc):
	echo H::msgBox('<h3>Dados para depósito não foram configurados.</h3>', false, H::DANGER);
elseif($purchase->purchase_vrc_method == 'deposit' && $status == 'payment_check'):		
	$chStatusUrl = URL::link(H::module(), 'deposited', URL::friend(2)) . '?hash=' . urlencode($purchase->purchase_vrc_hash);
	echo sprintf('<h3>Dados para depósito</h3><pre>%s</pre><br/>',$deposit_acc);
	
	echo H::msgBox(sprintf('
		<h4>Você já efetuou o deposito?</h4>
		<p><a href="%s" class="h4">Coloque o comprovante da compra no comentário e depois clique aqui.</a></p>
		', $chStatusUrl), false, H::WARNING);
endif;

echo '<div>';
include('purchase-items-table.php');
echo '</div>';
include('views/customer/single_address.php');
echo '<div class="clear"></div>';
if($purchase->purchase_status_vrc_alias == 'payment_denied'):
	#foreach($methods as $k=>$m)
	#	if($methods[$k]->method == $purchase->purchase_vrc_method)
	#		unset($methods[$k]);
		
	echo '<div style="border: 1px solid #ccc;padding: 10px;">';
			echo '<h4 class="text-center">Selecione outra forma de pagamento</h4>';
			if(!count($methods)) echo '<p class="text-center">Nenhuma forma de pagamento alternativa encontrada.</p>';
			
			foreach($methods as $m):
				printf('
					<a href="%1$s" title="%2$s" class="col-xs-2">
						<img src="/attachments/%2$s.png" alt="%2$s" title="%2$s" style="width: 100%%;">
					</a>
					',
					$m->url, 
					$m->method
				);
			endforeach;
			echo '<div class="clear"></div></div><br/>';
		
	printf('<a href="%s" class="btn btn-danger btn-block">Cancelar Compra</a>', 
		URL::root(). 'purchase/cancel/' . URL::friend(2) . '?hash=' . $purchase->purchase_vrc_hash) ;
	echo '</div>';
endif;

include('views/customer/form-warnings.php');

echo '
<div class="panel panel-smart">
	<div class="panel-heading">
		<h3 class="panel-title">
			Painel de chamado
		</h3>
	</div>
	<div class="panel-body">';

	printf('<form action="%s" enctype="multipart/form-data" method="post" class="default-form form-horizontal" id="form-comment">',
		URL::link('purchase','details', URL::friend(2))
	);

	echo '
		';

	if(!in_array($purchase->purchase_status_vrc_alias, array('completed', 'canceled'))):
		echo '
			<div class="field form-group">
				<label for="field-comment control-label">Digite um comentário</label>
				<textarea maxlength="1200" placeholder="Digite aqui" class="not_null form-control" id="field-comment" name="comment" style="height: 110px;width: 100%;"></textarea>
			</div>
			<div class="field form-group">
				<input type="text" readonly="readonly" value="" class="show_file_name form-control col-sm-9"/>
				<span class="btn btn-default btn-file col-sm-3">
					Browse <input type="file" name="purchase_comment_vrc_file" id="purchase_comment_vrc_file" accept=".jpg,.jpeg,.gif,.bmp,.txt,.pdf" />
				</span>
			</div>
			<div class="field form-group">
				<button type="submit" class="btn btn-primary btn-block">
					<span><i class="fa fa-comment"></i> &nbsp;Clique aqui para Comentar</span>
				</button>
			</div>';
	endif;


	$template_comment = '<div class="field" style="margin: 5px 0;background-color: #f0f6f0;padding: 5px 20px;">
		<h4 class="text-%s" >%s <span class="pull-right">%s</span></h4>
		<p>%s</p>
		%s
	</div>';

	if(!$comments) echo '<h4 class="text-center">Nenhum comentário</h4>';
	foreach($comments as $comment):
		$ext = empty($comment->file) ? '' : pathinfo($comment->file, PATHINFO_EXTENSION);
		if(empty($comment->file)):
			$content_file = '';
		elseif(in_array(strtolower($ext), array('jpg', 'jpeg', 'png', 'bmp', 'gif'))):
			$content_file = sprintf('<a href="%1$s" target="_blank"><img src="%1$s" style="max-width: 300px;"/></a>', $path . '/' . $comment->file);
		else:
			$content_file = sprintf('<h4><a href="%s" target="_blank"><i class="fa fa-file-text"></i> Anexo %s</a></h4>', $path . '/' . $comment->file, strtoupper($ext));
		endif;

		printf($template_comment, 
			!$comment->me ? 'warning' : 'success' ,
			!$comment->me ? 'Lojista disse:' : 'Você disse:' ,
			CData::format('d/m/Y H:i', $comment->dtt_added),
			str_replace("\n",'</p><p>', $comment->comment),
			$content_file
		);
	endforeach;

	echo '</form>';
echo '
	</div>
</div>';
?>
<script>
$(document).ready(function(){
	$('#purchase_comment_vrc_file').change(function(){
		$('input.show_file_name').val($(this).val().substr($(this).val().lastIndexOf('\\') + 1));
	});
});
</script>