<?php
include('step-progress.php');

$step_title = '<h3 class="text-center">Passo 3 - Forma de entrega</h3>';
include('views/cart/cart.php');
echo '<div class="col-xs-6">';
include('views/customer/single_address.php');
echo '</div>';
echo '<div class="col-xs-6">';
	if(!$info_methods):
		echo '<br/>';
		echo H::msgBox('
			<div style="margin: 30px;" class="h4">Não foi possivel determinar nenhuma forma de envio viável para essa lista de compra</div>
			<div>Obs.: Correios aceitam produtos de até 30kg ou máximo de 105 cm em uma das medidas.</div>
		', false, H::DANGER);
	else:
		include('views/customer/form-warnings.php');
		printf('<form name="next-step" action="%s" method="GET" id="form-next-step" style="padding: 5px 20px;">', URL::link('purchase','step-3',URL::friend(2)));
		echo '<h4 class="text-center">Selecione uma forma de envio.</h4>';
		foreach($info_methods as $i=>$m):
			printf('
				<label class="col-xs-12">
					<input type="radio" name="shipping" value="%s" />
					<i class="fa fa-envelope"></i> &nbsp;%s - R$ %s
				</label>
				', $i, $m[0], number_format($m[1],2,',','.')
			);
		endforeach;
		printf('
			<button type="submit" class="btn btn-success btn-block text-left">
				<i class="fa fa-dollar"></i> &nbsp; Finalizar compra
			</button>
		');
	endif;
		#URL::link('customer','purchase-step-4', URL::friend(2), $i), $m[0], $m[1]);
echo '</form>';
echo '</div>';
?>
<div class="clear"></div>
<script type="text/javascript">
$(document).ready(function(){
	$("button[type=submit]").click(function(){
		if(!$("#form-next-step input[type=radio]:checked").length)
			validator.showErrorBox("#form-next-step", [$("#form-next-step h4").first().html()]);
		else
			$("#form-next-step").submit();
		
		return false;
	});
		
	
});
	
</script>