<div style="padding-top: 30px;">
	<div class="col-md-3 text-center">
		<h3 style="font-size: 110px;color: #eee;margin: -25px 0 0 0;"><i class="fa fa-shopping-cart"></i></h3>
		<h3 style="color: #008000;margin: 0;margin-top: -80px;font-size: 30px;">Finalizar Compra</h3>
	</div>
	<div class="col-md-9">
		<ul class="progress-step">
		<?php 
			$tpl = '
				<li %s>
					<i class="fa fa-%s"></i>
					<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNgYAAAAAMAASsJTYQAAAAASUVORK5CYII=">
					<span>%s</span>
				</li>
			';
			$stepIcon = array('key','map-marker','cube','dollar','check'); 
			$steps = array('Login','Endereço de Entrega','Forma de Entrega','Forma de Pagamento','Fim');
			foreach($steps as $k=>$s):
				$active = ($step >= $k);
				printf( 
					$tpl, 
					$active ? ' class="on"' : '', 
					$stepIcon[$k],
					$s
				);
			endforeach;
		?>
		<div class="clear"></div>
		</ul>
	</div>
	<div class="clear"></div>
</div>