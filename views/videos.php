<div id="page-header">
				
	<div class="container">
		<div class="row">
			<div class="span12">
				<h1 style="color: #fff;"><?php echo $_title;?></h1>
			</div><!-- end .span12 -->
			
		</div><!-- end .row -->
	</div><!-- end .container -->
	
</div>
<div class="container">
	<div class="row">
		<div class="span12">
			<div class="portfolio-grid three-cols gutter portfolio-isotope">
			<?php
			$tpl = '
				<div class="item">
					<div class="portfolio-item">
						<div class="portfolio-item-preview">			
							<img src="%3$s" alt="%1$s" title="%1$s">
							<div class="portfolio-item-overlay">
								<div class="portfolio-item-description">
									<h4>
									<a href="%2$s" title="%1$s" target="_blank" type="youtube" data-modal-id="popup">%1$s</a>
									</h4>
								</div><!-- end .portfolio-item-description -->
							</div><!-- end .portfolio-item-overlay -->
						</div><!-- end .portfolio-item-preview -->
					</div><!-- end .portfolio-item -->
				</div><!-- end .item -->';
			foreach($videos as $v):
				printf($tpl, $v->title, $v->url, $v->thumb);
			endforeach;
			?>
			</div><!-- end .portfolio-grid -->
		</div><!-- end .span12 -->
	</div><!-- end .row -->
</div>