
<!-- Slider Section Starts -->
<div class="slider">
	<div id="main-carousel" class="carousel slide" data-ride="carousel">
	<!-- Wrapper For Slides Starts -->
		<div class="carousel-inner">
			<?php 
			foreach($banner_home_imgs as $k=>$img):
				printf('
				<div class="item %s">
					<img src="%s" alt="%s" title="%s" class="img-responsive" />
				</div>',
				$k < 1 ? 'active' : '', H::root() . PATH_IMAGES . $img->img, $img->alt, $img->title
				);
			endforeach;
			?>
		</div>
	<!-- Wrapper For Slides Ends -->
	<!-- Controls Starts -->
		<a class="left carousel-control" href="#main-carousel" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		<a class="right carousel-control" href="#main-carousel" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
	<!-- Controls Ends -->
	</div>	
</div>
<!-- Slider Section Ends -->

<!-- Latest Products Starts -->
<section class="product-carousel">			
<!-- Heading Starts -->
	<h2 class="product-head">Adicionados Recentemente</h2>
<!-- Heading Ends -->
<!-- Products Row Starts -->
	<div class="row">
		<div class="col-xs-12">
		<!-- Product Carousel Starts -->
			<div id="owl-product" class="owl-carousel">
			<?php
			$tpl = '
				<div class="item">
					<div class="product-col">
						<div class="image text-center">
							<a href="{url}" title="{name}"><img src="{thumb}" alt="{name}" title="{name}" /></a>
						</div>
						<div class="caption">
							<h4><a href="{url}" title="{name}">{name}</a></h4>
							<!--div class="description">{summary}</div-->
							<div class="price text-right">
								<span class="price-old">{old_price}</span>
								<a href="{url}" title="{name}"><span class="price-new">R$ {price}</span></a>
								
							<div class="clear" ></div>
							</div>
						</div>
					</div>
				</div>';
			foreach($products as $p):
				$p->url = H::link('product', URL::build($p->name, $p->id));
				$p->price = number_format($p->price, 2, ',','.');
				$p->old_price = '';
				$path = substr($p->img, 0,-1 * strlen(basename($p->img)));
				$p->thumb  = H::root() . ImagePlugin::resize(PATH_IMAGES . $p->img, trim(PATH_THUMBS . $path,'/'), 220, 220, false);
				echo TPL::format($tpl, $p);
			endforeach;
			?>
			
			</div>
		<!-- Product Carousel Ends -->
		</div>
	</div>
<!-- Products Row Ends -->
</section>
<!-- Latest Products Ends -->
