var mini_cart_timeout = null;

function update_mini_cart(){
	var tmp_options = {	
		type: 'POST', 
		url: URL_AJAX_MINI_CART, 
		data: {}, 
		dataType: 'html'
	};
	
	tmp_options.success = function (html) {
		$('#cart').html(html);
	};
	
	$.ajax(tmp_options);
	return false;
}


$(document).ready(function(){
	$('.mini-cart').hide();	
	$('.btn-cart').each(function(){
		
		$(this).click(function(){
			var btnAdd = $(this);
			if(btnAdd.hasClass('disabled'))
				return false;
				
			btnAdd.addClass('disabled');
			
			var qtt = $('#field-qtt').length > 0 ? $('#field-qtt').val() : 1;
			var product_options = [];
			$('.product-info .option select').each(function(){
				product_options.push($(this).val());
			});
			var tmp_options = {	
				type: 'POST', 
				url: btnAdd.attr('url'), 
				data: { 'product_dcm_quantity' : qtt, 'product_options' : product_options}, 
				dataType: 'json'
			};
			
			tmp_options.success = function (json) {
				alert(json.msg);
				btnAdd.removeClass('disabled');
				update_mini_cart();
			};
			
			$.ajax(tmp_options);
			return false;
		});
		
	});

});