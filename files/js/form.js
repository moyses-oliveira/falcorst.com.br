$(document).ready(function () {
    globalFormConfig();
});

function globalFormConfig(elm) {

    elm = (typeof elm === "undefined") ? '' : elm + ' ';


    var colocar_asterisco = [
        elm + ' .default-form input.not_null',
        elm + ' .default-form select.not_null',
        elm + ' .default-form textarea.not_null'
    ].join(', ');

    $(colocar_asterisco).each(function () {
        $(this).parent().find('label').addClass('required');
    });

    if (typeof $("#_TEST_").datepicker != 'undefined') {
		$(elm + 'input[type=text].datetime').each(function(){ $(this).addClass('date').removeClass('datetime') });
		$(elm + ' input.date').each(function () {
			var curYear = (new Date()).getFullYear();
			var range = (curYear - 115) + ':' + (curYear);
			if ($(this).hasClass('recent'))
				range = (curYear - 10) + ':' + (curYear);
			else if ($(this).hasClass('newest'))
				range = (curYear) + ':' + (curYear + 10);
			$(this).datepicker({changeMonth: true, changeYear: true, yearRange: range});
		});
	}


    $(elm + '.default-form').each(function () {
        var form = '#' + $(this).attr('id') + '';

        $(this).submit(function () {
            if (!validator.formValidate(this)) return false;
				
            if ($(this).hasClass('update_target')) {
                $(this).find("button[type=submit]").attr("disabled", "disabled");
                $(".close").attr("disabled", "disabled");
                $(this).find("button[type=submit]").html("<span></span>")
                $(this).find("button[type=submit] span").addClass("fa fa-spinner rotating");
                var tmp_target = $(this).attr('target');
                var tmp_options = {type: 'POST', url: $(this).attr('action'), data: $(this).serialize(), dataType: 'html'};
                tmp_options.success = function (html) {
                    $(tmp_target).html(html);
                    $(".close").removeAttr('disabled');
					globalFormConfig(tmp_target);
					
					var danger_tag = $(form).parent().find('.box-danger .box-body').first();
					$('html,body').animate({scrollTop: danger_tag.offset().top - 100}, 'slow');
					
                };
                $.ajax(tmp_options);
                return false;
            }

            return true;
        });
    });

    $(elm + '.default-form input[type=text],' +
            elm + '.default-form input[type=password],' +
            elm + '.default-form select, ' +
            elm + '.default-form textarea'
            ).each(function () {
        $(this).keyup(function (e) {
            validator.fieldValidate(this);
        });
        $(this).change(function (e) {
            validator.fieldValidate(this);
        });
    });
    $(elm + '.fone').addClass('number_mask');
    $(elm + '.fone').attr('mask', '(**) ****-*****').attr('placeholder', '(xx) xxxx-xxxxx');
    $(elm + '.cep').addClass('number_mask');
    $(elm + '.cep').attr('mask', '**.***-***').attr('placeholder', 'xx.xxx-xxx');
    $(elm + '.cpf').addClass('number_mask');
    $(elm + '.cpf').attr('mask', '***.***.***-**').attr('placeholder', 'xxx.xxx.xxx-xx');
    $(elm + '.cnpj').addClass('number_mask');
    $(elm + '.cnpj').attr('mask', '***.***.***/****-**').attr('placeholder', 'xxx.xxx.xxx/xxxx-xx');
    $(elm + '.rg').addClass('number_mask');
    $(elm + '.rg').attr('mask', '************').attr('placeholder', 'xxxxxxxxx');
	
	$(elm + 'input[type=text].int').each(function(){ $(this).addClass('integer').removeClass('int') });
	
    $(elm + '.integer').addClass('number_mask');
    $(elm + '.integer').attr('mask', '**********');


    $(elm + '.time').addClass('number_mask');
    $(elm + '.time').attr('mask', '**:**').attr('placeholder', '00:00');

    $(elm + '.date').addClass('number_mask');
    $(elm + '.date').attr('mask', '**/**/****').attr('placeholder', 'xx/xx/xxxx');

    $(elm + '.alphanum_mask').attr('mask', '******************************');
    $(elm + '.alpha_mask').attr('mask', '******************************');
    $(elm + '.alpha_plus_mask').attr('mask', '******************************');
    $(elm + '.alphanum_plus_mask').attr('mask', '******************************');

    if (typeof $("#_TEST_").maskMoney != 'undefined') {
		['percent','decimal_1','decimal_2','decimal_3'].forEach(function(cls, k) {
			var places = !k ? 1 : k;
			var placeholder = places == 1 ? '00.0' : (0).toFixed(k);
			$(elm + 'input[type=text].' + cls).each(function (i) {
				$(this).maskMoney({symbol: '', decimal: '.', thousands: '', precision: places, allowZero: !$(this).hasClass('not_zero')});
				$(this).attr('placeholder', placeholder).bind("contextmenu", function () { return false; });
			});
		});
    }
    if (typeof $("#_TEST_").chosen != 'undefined') {
        $(elm + '.chosen-select').chosen({width: '100%', no_results_text: 'Nenhum item encontrado!'});
    }

    if (typeof $("#_TEST_").formModal != 'undefined' && elm.length > 0) {
        $(elm + '.load_form_modal').formModal();
    }

    $(elm + '.uploader input[type=file]').each(function () {
        var t = $(this).parent().find('input[type=text]');
        $(this).change(function () {
            var val = $(this).val();
            if (val.lastIndexOf('/') > -1)
                val = val.substr(val.lastIndexOf('/'));
            t.attr('value', val);
        });
    });
     $(elm + '.btn-busca-cep').click(function (e) {
		e.preventDefault();
		var btnBuscaCep = $(this);
		btnBuscaCep.find('i').attr('class','fa fa-spinner rotating');
        var prefix = 'addr-address_';
		var form = $(this).parents('form:first');
        var submitBtn = form.find('button[type=submit]:last');
        var item = [];
        item[0] = 'vrc_postcode';
        item[1] = 'vrc_address';
        item[2] = 'vrc_neighborhood';
        item[3] = 'vrc_city';
        item[4] = 'int_zone';
        var valor_cep = $('#' + prefix + item[0]).val();
		
        var fields = {
            cep: $('#' + prefix + item[0]),
            logadouro: $('#' + prefix + item[1]),
            bairro: $('#' + prefix + item[2]),
            cidade: $('#' + prefix + item[3]),
            uf: $('#' + prefix + item[4])
        }
		
        if (valor_cep.length > 7) {
            $.each(fields, function (k, i) {
                fields[k].attr('disabled', 'disabled');
                fields[k].addClass('bg-gray');
            });
            submitBtn.attr('disabled', 'disabled');
            $.getJSON(ROOT + 'customer/get-cep/?cep=' + valor_cep, function (data) {
                
				data.uf = fields.uf.find('option[code=' + data.uf + ']').attr('value');
				
				if (data.cidade || data.bairro || data.logadouro) {
                    $.each(fields, function (k, i) {
                        if (k != 'cep')
                            fields[k].val(data[k]);
                    });
                } else {

                    $.each(fields, function (k, i) {
                        if (k != 'cep')
                            fields[k].val('');
                    });
                    alert(data.msg);
                }
                $.each(fields, function (k, i) {
                    fields[k].removeAttr('disabled');
                    fields[k].removeClass('bg-gray');
                });
                submitBtn.removeAttr('disabled');
				form.find('.invalid').removeClass('invalid');
				btnBuscaCep.find('i').attr('class','fa fa-search');
            });
			
        } else {
            alert('CEP inválido.');
			btnBuscaCep.find('i').attr('class','fa fa-search');
        }
        return false;
    });
	
    maskInput.init();
}

var validator = {
    vTypes: ['not_zero', 'not_null', 'not_null_no_trim', 'percent', 'decimal_2', 'decimal_3', 'email', 'fone', 'cep', 'date', 'time', 'regex', 'cnpj', 'cpf', 'min_value', 'max_value', 'css_unit', 'pass'],
    warnings: {
        not_zero: 'O campo {0} é obrigatório.',
        not_null: 'O campo {0} é obrigatório.',
        not_null_no_trim: 'O campo {0} é obrigatório.',
        email: 'O valor do campo {0} é inválido.',
        percent: 'O valor do campo {0} é inválido.',
        decimal_2: 'O valor do campo {0} é inválido.',
        decimal_3: 'O valor do campo {0} é invalido.',
        cpf: 'O valor do campo {0} é invalido.',
        fone: 'O valor do campo {0} é inválido.',
        cep: 'O valor do campo {0} é inválido.',
        regex: 'O valor do campo {0} é inválido.',
        date: 'O valor do campo {0} é inválido.',
        time: 'O valor do campo {0} é inválido.',
        min_value: 'O valor do campo {0} é inválido.',
        max_value: 'O valor do campo {0} é inválido.',
        css_unit: 'O valor do campo {0} é inválido.',
		pass: 'O campo {0} deve conter 6 ou mais caracteres.'
    },
    formValidate: function (form_el, arMsg) {
        if (typeof arMsg == 'undefined')
            arMsg = [];

        var form = '#' + $(form_el).attr('id') + '';

		$( 
			form + ' input[type=text]:enabled,' +
			form + ' input[type=password]:enabled,' +
			form + ' input[type=checkbox]:enabled,' +
			form + ' select:enabled,' +
			form + ' textarea:enabled'
		).each(function () {
            var field = this;
            var isValid = true;
            validator.changeStatus(field, true);
            validator.vTypes.forEach(function (type) {
                if ($(field).hasClass(type)) {
                    if (isValid) {
                        isValid = validator.validType(field, type);
                        if (!isValid) {
                            validator.changeStatus(field, isValid);
                            arMsg.push(validator.warnings[type].replace('{0}', $(field).parent().children('label').html()));
                        }
                    }
                }
            });
        });
        if (arMsg.length > 0) {
            validator.showErrorBox(form, arMsg);
            return false;
        }
        return true;

    },
    fieldsValidate: function (form_el, fields, arMsg) {
        if (typeof arMsg == 'undefined')
            arMsg = [];
        
        if(typeof fields == 'undefined') {
        	new Error('Undefined value for fields');
        	return false;
        }

        var form = $(form_el);
        $(fields).each(function (i, field) {
        	field = form.find(field);
        	var isValid = true;
            validator.changeStatus(field, true);
            validator.vTypes.forEach(function (type) {
                if (field.hasClass(type)) {
                    if (isValid) {
                        isValid = validator.validType(field, type);
                        if (!isValid) {
                            validator.changeStatus(field, isValid);
                            arMsg.push(validator.warnings[type].replace('{0}', field.parent().children('label').html()));
                        }
                    }
                }
            });
        });
        
        if (arMsg.length > 0) {
            validator.showErrorBox(form, arMsg);
            return false;
        }
        return true;

    },
    fieldValidate: function (field) {
        var isValid = true;
        validator.vTypes.forEach(function (type) {
            if ($(field).hasClass(type)) {
                if (isValid)
                    isValid = validator.validType(field, type);
            }
        });
        validator.changeStatus(field, isValid);
        return isValid;
    },
    validType: function (el, type) {
        var isValid = true;
        if (type == 'regex') {
            return validator.test.regex($(el).val(), $(el).attr('expression'));
        }

        var test = eval('validator.test.' + type);
        if (typeof test == 'function')
            return test($(el).val(), el);

        console.log('test is not a function');
    },
    test: {
        regex: function (v, exp) {
            if ((v + '') == '')
                return true;
            var expression = eval(exp);
            if (!(expression instanceof RegExp)) {
                console.log('Is not RegExp.');
                return true;
            }
            return expression.test(v);
        },
        css_unit: function (v) {
            return validator.test.regex(v, '/(^([0-9]+|[0-9]+\.[0-9]+)(em|ex|%|px|cm|mm|in|pt|pc|vh|vw)$|^0$)/');
        },
        percent: function (v) {
            return validator.test.regex(v, '/^\\d+\\.\\d{1}$/');
        },
        decimal_2: function (v) {
            return validator.test.regex(v, '/^\\d+\\.\\d{2}$/');
        },
        decimal_3: function (v) {
            return validator.test.regex(v, '/^\\d+\\.\\d{3}$/');
        },
        email: function (v) {
            return validator.test.regex(v, '/\\S+@\\S+\\.\\S+/');
        },
        fone: function (v) {
            return validator.test.regex(v, '/^(\\([0-9][0-9]\\) [0-9]{4}-([0-9]{4}|[0-9]{5}))$/');
        },
        cep: function (v) {
            return validator.test.regex(v, '/^([0-9]{2}.[0-9]{3}-[0-9]{3})$/');
        },
		pass: function(v){
			return v.length > 5;
		},
        cnpj: function (v) {
            if ((v + '') == '')
                return true;
				
            v = v.replace(new RegExp('[./\-]', 'g'), '');
            var cnpj = v;
            var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
            digitos_iguais = 1;
            if (cnpj.length < 14)
                return false;
            for (i = 0; i < cnpj.length - 1; i++)
                if (cnpj.charAt(i) != cnpj.charAt(i + 1))
                {
                    digitos_iguais = 0;
                    break;
                }
            if (!digitos_iguais)
            {
                tamanho = cnpj.length - 2
                numeros = cnpj.substring(0, tamanho);
                digitos = cnpj.substring(tamanho);
                soma = 0;
                pos = tamanho - 7;
                for (i = tamanho; i >= 1; i--)
                {
                    soma += numeros.charAt(tamanho - i) * pos--;
                    if (pos < 2)
                        pos = 9;
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(0))
                    return false;
                tamanho = tamanho + 1;
                numeros = cnpj.substring(0, tamanho);
                soma = 0;
                pos = tamanho - 7;
                for (i = tamanho; i >= 1; i--)
                {
                    soma += numeros.charAt(tamanho - i) * pos--;
                    if (pos < 2)
                        pos = 9;
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(1))
                    return false;
                return true;
            }
            else
                return false;

//            return validator.test.regex(v, '/^\\d{2,3}.\\d{3}.\\d{3}' + '\\' + '/\\d{4}-\\d{2}$/');
        },
        cpf: function (v) {
            if ((v + '') == '')
                return true;
				
            v = v.replace(new RegExp('[.-]', 'g'), '');
            var numeros, digitos, soma, i, resultado, digitos_iguais;
            digitos_iguais = 1;
            if (v.length < 11)
                return false;
            for (i = 0; i < v.length - 1; i++)
                if (v.charAt(i) != v.charAt(i + 1))
                {
                    digitos_iguais = 0;
                    break;
                }
            if (!digitos_iguais)
            {
                numeros = v.substring(0, 9);
                digitos = v.substring(9);
                soma = 0;
                for (i = 10; i > 1; i--)
                    soma += numeros.charAt(10 - i) * i;
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(0))
                    return false;
                numeros = v.substring(0, 10);
                soma = 0;
                for (i = 11; i > 1; i--)
                    soma += numeros.charAt(11 - i) * i;
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(1))
                    return false;
                return true;
                ;
            }
            else
                return false;

        },
        date: function (v) {
            if ((v + '').length < 1)
                return true;
            var exp = /^(0?[1-9]|[12][0-9]|3[01])[\/\-\.](0?[1-9]|1[012])[\/\-\.](\d{4})$/;
            if (!exp.test(v))
                return false;
            var date_time = v.split('/');
            var Y = date_time[2] * 1;
            var M = (date_time[1] * 1) - 1;
            var D = date_time[0] * 1;
            var dt = new Date(Y, M, D);
            var today = new Date();
            if (Y > (today.getUTCFullYear() + 10))
                return false;
            if (Y < (today.getUTCFullYear() - 120))
                return false;
            if (dt.getMonth() != M)
                return false;
            if (dt.getUTCFullYear() != Y)
                return false;
            return true;
        },
        time: function (v) {
            if ((v + '').length < 1)
                return true;
            var exp = /^([0-1][0-9]|2[0-3]):[0-5][0-9]$/;
            return exp.test(v);
        },
        not_null: function (v, trim) {
            var trim = typeof trim == 'undefined' ? true : trim;
            if (typeof v == 'undefined') {
                console.log('Value is undefined.');
                return false;
            }
            return (v + '').trim() != '';
        },
        not_zero: function (v, trim) {
            var trim = typeof trim == 'undefined' ? true : trim;
            if (typeof v == 'undefined') {
                console.log('Value is undefined.');
                return false;
            }
            v = (v + '').trim() == '' ? 0 : (v + '').trim();
            return parseFloat(v);
        },
        not_null_no_trim: function (v) {
            return validator.test.not_null(v, false);
        },
        min_value: function (v, el) {
        	var m = $(el).attr('min-value');
        	if(typeof m != 'undefined' && m!= '' && v != '')
        		return (v * 1.00) >= (m * 1.00);
        	return true;
        },
        max_value: function (v, el) {
        	var m = $(el).attr('max-value');
        	if(typeof m != 'undefined' && m!= '' && v != '')
        		return (v * 1.00) <= (m * 1.00);
        	return true;
        }
    },
    showErrorBox: function (form, arMsg) {
		if($(form).prop("tagName").toLowerCase() != 'form')
			form = '#' + $(form).closest('form').attr('id');
			
        $(form).parent().find('.box-danger').attr('style', '');
        var msg = '<ul><li>' + arMsg.join('</li><li>') + '</li></ul>';
        var danger_tag = $(form).parent().find('.box-danger .box-body').first();
        danger_tag.html(msg);
		// MOVE A BARRA DE ROLAGEM DA MODAL PARA A POSIÇÃO DA MENSAGEM DE ERRO
		var div_overflow = danger_tag.parents('.modal-body>div').first();
		if(div_overflow.length > 0) {
			if(div_overflow.css('overflow') == 'auto') { 
				div_overflow.animate({scrollTop: 0}, 'slow');
				return false;
			} 
		}
		// MOVE A BARRA DE ROLAGEM DA PÁGINA PARA A POSIÇÃO DA MENSAGEM DE ERRO
        $('html,body').animate({scrollTop: danger_tag.offset().top - 100}, 'slow');
    },
    changeStatus: function (el, valid) {
        if (valid)
            $(el).removeClass('invalid');
        else
            $(el).addClass('invalid');
        return valid;
    }
}

var maskInput = {
    init: function () {
        $('input.number_mask').not('.cnpj , .cpf').keydown(function (e) {
            return maskInput.keyCodeTest.number(e.keyCode);
        });
        $('input.number_mask').not('.cnpj , .cpf').keyup(function (e) {
            if (maskInput.keyCodes(e.keyCode, [8, 9]))
                return false;
            maskInput.keyUpTest(this, /^\d+$/);
            if (!maskInput.keyCodeTest.number(e.keyCode))
                return false;
            validator.fieldValidate(this);
        });

        $('input.number_mask').filter('.cnpj, .cpf').keydown(function (e) {
            return maskInput.keyCodeTest.number(e.keyCode);
        });
        $('input.number_mask').filter('.cnpj, .cpf').keyup(function (e) {
            if (maskInput.keyCodes(e.keyCode, [8, 9]))
                return false;
            maskInput.keyUpTest(this, /^\d+$/, true);
            if (!maskInput.keyCodeTest.number(e.keyCode))
                return false;
            validator.fieldValidate(this);
        });

        $('input.alphanum_mask').keydown(function (e) {
            return maskInput.keyCodeTest.alphanum(e.keyCode);
        });

        $('input.alphanum_mask').keyup(function (e) {
            if (maskInput.keyCodes(e.keyCode, [8, 9]))
                return false;
            maskInput.keyUpTest(this, /^[a-z0-9]+$/i);
            if (!maskInput.keyCodeTest.alphanum(e.keyCode))
                return false;
            validator.fieldValidate(this);
        });

        $('input.alphanum_plus_mask').keydown(function (e) {
            return maskInput.keyCodeTest.alphanum_plus(e.keyCode);
        });

        $('input.alphanum_plus_mask').keyup(function (e) {
            if (maskInput.keyCodes(e.keyCode, [8, 9]))
                return false;
            maskInput.keyUpTest(this, /^[a-z0-9\.\-_]+$/i);
            if (!maskInput.keyCodeTest.alphanum_plus(e.keyCode))
                return false;
            validator.fieldValidate(this);
        });

        $('input.alpha_mask').keydown(function (e) {
            return maskInput.keyCodeTest.alpha(e.keyCode);
        });

        $('input.alpha_mask').keyup(function (e) {
            if (maskInput.keyCodes(e.keyCode, [8, 9]))
                return false;
            maskInput.keyUpTest(this, /^[a-z]+$/i);
            if (!maskInput.keyCodeTest.alpha(e.keyCode))
                return false;
            validator.fieldValidate(this);
        });

        $('input.alpha_plus_mask').keydown(function (e) {
            return maskInput.keyCodeTest.alpha_plus(e.keyCode);
        });

        $('input.alpha_plus_mask').keyup(function (e) {
            if (maskInput.keyCodes(e.keyCode, [8, 9]))
                return false;
            maskInput.keyUpTest(this, /^[a-z\.\-_]+$/i);
            if (!maskInput.keyCodeTest.alpha_plus(e.keyCode))
                return false;
            validator.fieldValidate(this);
        });

    },
    keyUpTest: function (field, regex, reverse) {

        reverse = reverse || false;
        var value = $(field).val();
        var char = value.split('').pop();
        var fValue = '';
        value.split('').forEach(function (c) {
            if ((regex).test(c))
                fValue += (c + '');
        });

        var rawValue = $(field).attr('mask') ? $(field).attr('mask') : '(**) ****-*****';
        var rawLength = rawValue.length;
        if (reverse)
        {
            rawValue = rawValue.split('').reverse().join('');
            fValue.split('').reverse().forEach(function (c) {
                rawValue = rawValue.replace('*', c);
            });
            rawValue = rawValue.split('').reverse().join('');
        }
        else
            fValue.split('').forEach(function (c) {
                rawValue = rawValue.replace('*', c);
            });

        rawValue = rawValue.replace(new RegExp('\\*', 'g'), '');
        $(field).val(rawValue.substr(0, rawLength));

    },
    keyCodeTest: {
        alpha_plus: function (num) {
            return maskInput.rangeTest(num, 65, 90) || maskInput.keyCodes(num, [8, 9, 190, 189, 109]);
        },
        alpha: function (num) {
            return maskInput.rangeTest(num, 65, 90) || maskInput.keyCodes(num, [8, 9]);
        },
        alphanum: function (num) {
            return maskInput.rangeTest(num, 96, 105) || maskInput.rangeTest(num, 48, 57) || maskInput.rangeTest(num, 65, 90) || maskInput.keyCodes(num, [8, 9]);
        },
        alphanum_plus: function (num) {
            return maskInput.rangeTest(num, 96, 105) || maskInput.rangeTest(num, 48, 57) || maskInput.rangeTest(num, 65, 90) || maskInput.keyCodes(num, [8, 9, 190, 189, 109]);
        },
        number: function (num) {
            return maskInput.rangeTest(num, 96, 105) || maskInput.rangeTest(num, 48, 57) || maskInput.keyCodes(num, [8, 9]);
        }
    },
    rangeTest: function (num, start, end) {
        return num >= start && num <= end;
    },
    keyCodes: function (key, keys) {
        return keys.indexOf(key) != -1;
    }
};

var kkeys = [], konami = "38,38,40,40,37,39,37,39,66,65";
$(document).keydown(function (e) {
    kkeys.push(e.keyCode);
    if (kkeys.toString().indexOf(konami) >= 0) {
        $(document).unbind('keydown', arguments.callee);
//        $('body').glitch('replace', {});
        $('body').addClass('animated jello');
    }
});