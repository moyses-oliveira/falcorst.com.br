/* wait for images to load */
$(window).load(function() {
	$("#share-product").share({
		networks: ["facebook","googleplus","twitter","linkedin","email"],
		theme: "square"
	});
	$(".qtt-action").each(function(){
		$(this).click(function(){
			var qtt = $(this).parent().find("input[name=qtt]");
			if($(this).hasClass("disabled"))
				return false;
			
			var operador = ($(this).attr("value") * 1);
			qtt.val((qtt.val() * 1) + operador);
			
			if(qtt.val() == 1)
				$(this).parent().find(".qtt-action.less").addClass("disabled");
			else
				$(this).parent().find(".qtt-action.less").removeClass("disabled");
						
			return false;
		});
	});
	
	$('#form-shipping').submit(function(){
		
		var data = {};
		$(this).find('input').each(function() {
			data[$(this).attr('name')] = $(this).val();
		});
		
		if(data.cep.length < 8) {
			alert('Cep inválido');	
			return false;
		}
		
		data.qtt =  $('#field-qtt').val();
		$("#form-shipping button[type=submit]").attr('disabled','disabled');
		$("#form-shipping button[type=submit]").find('i').first().attr('class','fa fa-spinner rotating');
		
		
		var options = {	type: 'GET', url: $(this).attr('action'), data: data, dataType: 'html'};
		options.success = function (html) {
			$("#form-shipping button[type=submit]").removeAttr('disabled');
			$("#form-shipping button[type=submit]").find('i').first().attr('class','fa fa-truck');
			try {
				var json = jQuery.parseJSON(html);
				if(!json.success) {
					alert('Cep inválido');
					return false;
				}
				var content = '<div class="text-center"><p>' + json.addr.uf + ' - ' + json.addr.cidade + ' - ' + json.addr.bairro + '</p>';
				content +=  '<p>Logadouro: ' + json.addr.logadouro + '</p>';
				content += '<div class="table-responsive"><table  class="table">';
				content += '<tr><th>Tipo de Frete</th><th>Preço R$</th></tr>';
				$.each(json.prices, function(k,p) {
					content += '<tr><td>' + p[0] + '</td><td>' + p[1]  + '</td></tr>';			
				});
				content += '</table></div>';
				$('.calc-shipping-content').html(content);
			}
			catch(err) {
				alert('Erro');
			}
		};
		$.ajax(options);
		return false;
	});
	
	$("input[name=cep]").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			 // Allow: Ctrl+A
			(e.keyCode == 65 && e.ctrlKey === true) ||
			 // Allow: Ctrl+C
			(e.keyCode == 67 && e.ctrlKey === true) ||
			 // Allow: Ctrl+X
			(e.keyCode == 88 && e.ctrlKey === true) ||
			 // Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)) {
				 // let it happen, don't do anything
				 return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	
	if ( typeof $.fn.zoom !== undefined && typeof $.fn.zoom != 'undefined' )
	{
		$('.images-block .img-zoom').zoom({ on:'grab' });
		$('.images-block ul > li > a').on('click', function(e) {
			e.preventDefault();
			$('.images-block .img-zoom img').attr('src', $(this).attr('href'));
		});
	}
});