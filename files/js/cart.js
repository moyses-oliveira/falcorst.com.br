$(document).ready( function(){

	$('.remove-from-cart').each(function(){
		$(this).unbind( "click");
		$(this).click(function(){
			var btn = $(this);
			if(confirm('Deseja excluir o item?')) {
				var tmp_options = {	type: 'POST', url: $(this).attr('href'), data: {}, dataType: 'html'};
				tmp_options.success = function (html) {
					try {
						var json = jQuery.parseJSON(html);
						if(!json.success)
							alert(json.msg);
					}
					catch(err) {
						window.location.href = URL_CURRENT;
					}
					
				};
				$.ajax(tmp_options);
			}
			return false;
		});
		
	});
		
	$('#shoppingCartModal .image-modal-viewer').hide();
	
	$('div.customer-content .image-modal-viewer').each(function(){
		$(this).unbind( "click");
		$(this).click(function(){
			$('#modal_image .modal-body img').css('background-image','url(' + $(this).attr('href') + ')');
			$('#modal_image .modal-header h4').html($(this).attr('title'));
			$('#modal_image').modal('show');
			return false;
		});
	});
	
	$('#form-shipping').submit(function(){
		var data = {};
		$(this).find('input').each(function() {
			data[$(this).attr('name')] = $(this).val();
		});
		
		if(data.cep.length < 8) {
			alert('Cep inválido');	
			return false;
		}
		
		$("#form-shipping button[type=submit]").attr('disabled','disabled');
		$("#form-shipping button[type=submit]").find('i').first().attr('class','fa fa-spinner rotating');
		var options = {	type: 'GET', url: $(this).attr('action'), data: data, dataType: 'html'};
		options.success = function (html) {
			$("#form-shipping button[type=submit]").removeAttr('disabled');
			$("#form-shipping button[type=submit]").find('i').first().attr('class','fa fa-truck');
			try {
				var json = jQuery.parseJSON(html);
				if(!json.success) {
					alert('Cep inválido');
					return false;
				}
				var content = '<div class="text-center" style="margin: 10px 0;"><p>' + json.addr.uf + ' - ' + json.addr.cidade + ' - ' + json.addr.bairro + '</p>';
				content +=  '<p>Logadouro: ' + json.addr.logadouro + '</p></div>';
				content += '<div class="table-responsive"><table  class="table">';
				content += '<tr><th>Tipo de Frete</th><th>Preço R$</th></tr>';
				$.each(json.prices, function(k,p) {
					content += '<tr><td>' + p[0] + '</td><td>' + p[1]  + '</td></tr>';			
				});
				content += '</table></div>';
				$('.calc-shipping-content').html(content);
			}
			catch(err) {
				alert('Cep inválido');
			}
			
		};
		
		$.ajax(options);
		return false;
	});
	
	$("input[name=cep]").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			 // Allow: Ctrl+A
			(e.keyCode == 65 && e.ctrlKey === true) ||
			 // Allow: Ctrl+C
			(e.keyCode == 67 && e.ctrlKey === true) ||
			 // Allow: Ctrl+X
			(e.keyCode == 88 && e.ctrlKey === true) ||
			 // Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)) {
				 // let it happen, don't do anything
				 return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	
});