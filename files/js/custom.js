//MAGNIFIC POPUP

$(document).ready(function() {
	$('meta[type=js]').each(function(){
		window[$(this).attr('name')] = $(this).attr('content');
	});
	
	$("#footer-share").share({
		networks: ["facebook","googleplus","twitter","linkedin","email"],
		theme: 'square',
        urlToShare: document.location.origin
	});
	
	$('.shopping-cart-table .product-name').magnificPopup({ delegate: 'a', type: 'image', gallery: { enabled: true } });

	// TOOLTIP	
	$(".header-links .fa, .tool-tip").tooltip({ placement: "bottom" });
	
	$(".btn-wishlist, .btn-compare, .display .fa").tooltip('hide');

	// Product Owl Carousel
	$("#owl-product").owlCarousel({
		autoPlay: false, //Set AutoPlay to 3 seconds
		items : 4,
		stopOnHover : true,
		navigation : true, // Show next and prev buttons
		pagination : false,
		navigationText : ["<span class='glyphicon glyphicon-chevron-left'></span>","<span class='glyphicon glyphicon-chevron-right'></span>"]
	});

	// TABS
	$('.nav-tabs a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	});
	$('#search form').submit(function(){
		var search_input = $('#search form input[name=search]');
		if(search_input.val().length > 0 && search_input.val().length < 3){
			alert('Não é possivel buscar palavras com menos que 3 caracteres.');
			return false;
		}
	});
});
