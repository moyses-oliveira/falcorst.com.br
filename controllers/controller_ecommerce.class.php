<?php

class controller_ecommerce extends controller {
	
	public static $base = 'ecommerce';
	
	public static function _config(){
		H::css(array('css/smoothness/jquery-ui.css','css/datepicker3.css','css/customer.css'));
		H::js(array('jquery.ui.core.js', 'jquery.ui.datepicker.js', 'jquery.ui.datepicker-pt-BR.js'));
		static::$data->_show_customer_menu = true;
	}
	
	public static function setAction(){
		if(empty(URL::friend(1)))
			return 'index';
			
		return str_replace(array('-'),array('_'), URL::friend(1));
	}
	
	protected static function ConfirmationMail($mail, &$errors) {
		/* Pega o hash de confirmação usando o e-mail como base */
		$params = array();
		$_SESSION['confirm_pass'] = substr(md5(rand(0,999999)), 2, 8);
		$_SESSION['confirm_email'] = $mail;
		
		$params['str'] = $mail;
		$params['enc'] = '1';
		static::$api->addAction(static::$base, 'ec_crypt', 'run', $params);
		$res = static::$api->callMethod();
		$errors = $res->errors;
		
		/* Pega as informações sobre o cliente atravéz do e-mail */
		if(!$res->errors):
			$confirm_code = current($res->data)->res;
			static::$api->addAction(static::$base, 'ec_get_customer_by_email', 'run', array('vrc_email'=>$mail));
			$res = static::$api->callMethod();
			$errors = $res->errors;
		endif;
		
		/* Envia o e-mail de confirmação */
		if(!$res->errors):
			$customer = current($res->data);
			
			$confirmLink = sprintf('%s:%scustomer/confirm/?confirm_code=%s%s',$_SERVER['REQUEST_SCHEME'],URL::site(),urlencode($confirm_code), (isset($_GET['to']) ? '&to='.$_GET['to']  : '') );
			$confirmLabel = 'Confirmar seu e-mail.';
			$confirmPass = $_SESSION['confirm_pass'];
			$logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNgYAAAAAMAASsJTYQAAAAASUVORK5CYII=';
			$siteName = static::getSettings()->smtp_name;
			
			$tplFile = 'views/customer/confirm.mail.html';
			$target = base64_encode($_SERVER['SERVER_NAME']);
			$tplData = compact('logo','siteName', 'confirmLink', 'confirmLabel', 'target', 'confirmPass');
			$subj = 'E-mail de confirmação';
			static::MailFromTemplate(array(array($customer->email,$customer->name)), $subj, $tplFile, $tplData);
		endif;
	}
	
	protected static function getShipping($addr = null) {
		$disable = false;
		$value = 0;
		$settings = static::getSettings();
		
		$params = array('chr_language'=>'pt_br' );
		static::$api->addAction(static::$base,'ec_list_shipping','run', $params);
		$shipping_methods = static::$api->callMethod()->data;
		
		$cart_total_weight = 0; 
		foreach(static::$data->shopping_cart_data->items as $i)
			$cart_total_weight += ($i->quantity * $i->weight);
		
		if(!isset($addr)):
			$cep = static::$data->address->address_vrc_postcode;
			$zone = static::$data->address->address_vrc_zone;
			$params = array(
				'region_int_zone'=>static::$data->address->address_int_zone,
				'dcm_weight'=>$cart_total_weight
			);
			static::$api->addAction(static::$base,'ec_list_custom_shipping_by_zone','run', $params);
		else:
			$cep = $_GET['cep'];
			$zone = $addr->uf;
			$params = array(
				'region_chr_zone'=>$zone,
				'dcm_weight'=>$cart_total_weight
			);
			static::$api->addAction(static::$base,'ec_list_custom_shipping_by_zone_code','run', $params);
		endif;
		$res = static::$api->callMethod();
		$custom_shipping_methods = $res->data;
		
		static::$data->info_methods = array();
		
		foreach($shipping_methods as $shipping):
			if(!$shipping->enabled) continue;
			
			if($shipping->alias == 'custom'):
				foreach($custom_shipping_methods as $csm):
					$cod = 'custom_'.$csm->regw_int_id;
					$label = sprintf('%s - %s - %s', $shipping->label, $zone, $csm->region_vrc_name);
					static::$data->info_methods[$cod] = array($label,$csm->regw_dcm_price);
				endforeach;
			elseif($shipping->alias == 'fixed'):
				static::$data->info_methods['fixed'] = array($shipping->label, $settings->shipping_fixed);
			else:
				$value = 0;
				if(substr($shipping->alias,0,7) == 'correio'):
					$method = strtoupper(substr($shipping->alias,8));
					$cart = array();
					foreach(static::$data->shopping_cart_data->items as $k=>$prod):
						$item = array();
						foreach(array('length', 'width', 'height') as $m)
							$item[$m] = $prod->{$m} * 100;

						$item['weight'] = $prod->weight;
						$item['price'] = $prod->price;
						$item['quantity'] = $prod->quantity;
						$item['key'] = $prod->product_sub_int_id;
						$cart[] = $item;
					endforeach;
					$packages = correio::makePackages($cart);
					
					$to = str_replace(array('.','-'), '', $cep);
					$from = $settings->post_cep;
					$from= str_replace(array('.','-'), '', $from);
					
					foreach($packages as $p):
						$value += correio::frete(constant($method), $from, $to, $p['weight'], $p['length'], $p['width'], $p['height'], $p['price']);
					endforeach;
				endif;
				static::$data->info_methods[$shipping->alias] = array($shipping->label, $value);
			endif;
		endforeach;
	}
	
	protected static function singleAddress($address_int_id) {
		# Pegando endereço pelo id
		$params = array('customer_int_id' => MY_ID,'address_int_id' => $address_int_id);
		static::$api->addAction(static::$base, 'ec_get_address', 'run', $params);
		$res = static::$api->callMethod();
		$errors = $res->errors;
		if(!!$errors):
			URL::er500();
			die;
		endif;
		static::$data->address = current($res->data);
	}
	
	protected static function MailFromTemplate($mails, $subject, $tplFile, $tplData) {
		$tpl = file_get_contents($tplFile);
		$replaces = array();
		
		foreach($tplData as $k=>$v)
			$replaces['{'.$k.'}'] = $v;
		
		$msg = str_replace(array_keys($replaces), array_values($replaces), $tpl);
		static::sendNotification($mails, $subject, $msg);
	}
	
	protected static function addressData($isSelect = null){
		$params = array();
		$params['zone_int_country'] = 30;
		static::$api->addAction(static::$base, 'ec_select_zone', 'run', $params);
		$ZoneData = static::$api->callMethod();
		$errors = $ZoneData->errors;
		if(!!$errors):
			URL::er500();
			die;
		endif;

		$fields = array();
		$fields['address_vrc_postcode'] = (object)array('label'=>'CEP', 'type'=>array('not_null', 'cep'), 'value'=>'', 'true_value'=>'', 'length'=>11);
		$fields['address_int_country'] = (object)array('label'=>'País', 'type'=>array('not_null'), 'value'=>30, 'true_value'=>30);
		array_unshift($ZoneData->data, (object)array('key'=>'', 'code'=>'','value'=>'Selecione um Estado'));
		$fields['address_int_zone'] = (object)array('label'=>'Estado', 'type'=>array('not_null'), 'value'=>'', 'true_value'=>'', 'options'=>$ZoneData->data);
		$fields['address_vrc_city'] = (object)array('label'=>'Cidade', 'type'=>array('not_null'), 'value'=>'', 'true_value'=>'', 'length'=>255);
		$fields['address_vrc_neighborhood'] = (object)array('label'=>'Bairro', 'type'=>array('not_null'), 'value'=>'', 'true_value'=>'', 'length'=>255);
		$fields['address_vrc_address'] = (object)array('label'=>'Logadouro', 'type'=>array('not_null'), 'value'=>'', 'true_value'=>'');
		$fields['address_vrc_number'] = (object)array('label'=>'Número', 'type'=>array('not_null'), 'value'=>'', 'true_value'=>'', 'length'=>45);
		$fields['address_vrc_complement'] = (object)array('label'=>'Complemento', 'type'=>array(''), 'value'=>'', 'true_value'=>'', 'length'=>45);
		static::$data->prefix = 'addr';
		static::$data->fields = $fields;
		static::$data->errors = array();
		static::$data->select = $isSelect;
		
		static::$api->addAction(static::$base, 'ec_list_address', 'run', array('customer_int_id' => MY_ID));
		$res = static::$api->callMethod();
		if(!!$errors):
			URL::er500();
			die;
		endif;
			
		static::$data->addresses = $res->data;
		
		if(count($_POST)):
			// Somente Brasil, futuramente será implementado seleção de país
			$_POST[static::$data->prefix]['address_int_country'] = 30;
			
			static::validateFields(static::$data->fields, static::$data->errors, static::$data->prefix);
			
			if(!static::$data->errors):
				$params = $_POST[static::$data->prefix];
				$params['customer_int_id'] = MY_ID;
				$params['chr_flag'] = 'I';
				$params['address_int_id'] = null;
				static::$api->addAction(static::$base, 'ec_save_customer_address', 'run', $params);
				$res = static::$api->callMethod();
				static::$data->errors = $res->errors;

				if(!static::$data->errors): 
					H::redirect(H::module(),H::action());
				endif;
			endif;
		endif;
	}
	
	protected static function cartItems(){
		return static::$data->shopping_cart_data->items;
		
	}
	
	protected static function fieldsLogin(){
		$fields = array();
		$fields['vrc_email'] = (object)array('label'=>'E-mail','type'=>array('not_null', 'email'),'value'=>'', 'true_value'=>'');
		$fields['vrc_pass'] = (object)array('label'=>'Senha','type'=>array('not_null', 'pass'),'value'=>'', 'true_value'=>'');
		return $fields;
	}
	
	protected static function fieldsRegister(){
		static::$api->addAction(static::$base, 'ec_get_use_terms', 'run', array());
		$res = static::$api->callMethod()->data;
		static::$data->use_terms = $res[0]->terms;
		$fields = array();
		$fields['vrc_name'] = (object)array('label'=>'Nome','type'=>array('not_null'),'value'=>'');
		$fields['dte_birth'] = (object)array('label'=>'Data de Nascimento', 'type'=>array('not_null', 'date'), 'value'=>'', 'true_value'=>'');
		$fields['vrc_email'] = (object)array('label'=>'E-mail','type'=>array('not_null', 'email'),'value'=>'', 'true_value'=>'');
		$fields['vrc_pass'] = (object)array('label'=>'Senha','type'=>array('not_null', 'pass'),'value'=>'', 'true_value'=>'');
		$fields['vrc_re_pass'] = (object)array('label'=>'Repita a Senha','type'=>array('not_null', 'pass', 'compare'),'value'=>'', 'true_value'=>'');
		return $fields;
	}
	
	
	/**
	* Valida campos de formulário para evitar dados incorretos possui estrutura semelhante ao validador JavaScript
	*
	* @param array $fields Informação dos Campos à serem validados.
	* @param string $errors Variavel que retorna os erros
	*
	*/
	protected static function validateFields(&$fields, &$errors, $prefix = null){
		foreach($fields as $k=>&$f):
			$v = !$prefix ? $_POST[$k] : $_POST[$prefix][$k];
			$f->true_value = $v;
			
			if(in_array('date', $f->type) && !empty($v)):
				$f->true_value = !Validate::test('date', $v) || empty($v) ? null : date_format(DateTime::createFromFormat('d/m/Y',$v), 'Y-m-d');
			endif;
			if(!in_array('pass', $f->type)):
				$f->value = $v;
			endif;
			foreach($f->type as $t):
				if(!Validate::test($t, $v)):
					$errors[$k] = Validate::message($t, $f->label);
					break;
				endif;
			endforeach;
			if(!isset($errors[$k]) && in_array('pass', $f->type) && strlen($v) < 6 ):
				$errors[$k] = sprintf('O campo %s deve conter 6 ou mais caracteres.' , $f->label);
			endif;
			if(!isset($errors[$k]) && in_array('compare', $f->type) && $v != $_POST['vrc_pass']):
				$errors[$k] = sprintf('O campo %s não confere.' , $f->label);
			endif;
		endforeach;
	}
	
	/**
	* Envia e-mails através de naoresponda@liguesite.com.br com layout pré-definido.
	*
	* @param array $mails Lista de e-mails.
	* @param string $subject Assunto da mensagem.
	* @param string $msg Mensagem HTML.
	*
	* @return bool (Se erro retorna erro500)
	*/
	protected static function sendNotification($mails, $subject, $msg) {
		#require 'tools/phpmailer/class.phpmailer.php';
        try
        {
            // Inicia a classe PHPMailer
            $mail = new PHPMailer();


            // Define os dados do servidor e tipo de conexão
            $mail->IsSMTP();
            #$mail->SMTPDebug = 2;
            $mail->Host = self::$settings->smtp_host;
            $mail->Port = self::$settings->smtp_port;
			$mail->SMTPSecure = self::$settings->smtp_secure;
            $mail->SMTPAuth = true; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
            $mail->Username = self::$settings->smtp_email; // Endereço de email do usuário
            $mail->Password = self::$settings->smtp_password; // Senha de email do usuário

            // Remetente
            $mail->From = self::$settings->smtp_email; // Seu e-mail
            $mail->Sender = self::$settings->smtp_email; // Seu e-mail
            $mail->FromName = self::$settings->smtp_name; // Seu nome

            // Define os Destinatário
            foreach ($mails as $item_mail)
                $mail->AddAddress($item_mail[0], $item_mail[1]);


            // Tipo de mensagem
            $mail->IsHTML(true);
            $mail->CharSet = 'utf-8';


            // Define a mensagem (Texto e Assunto)
            $mail->Subject = $mail->FromName . ' - ' . $subject; // Assunto da mensagem
            $mail->Body = $msg;


            if (!$mail->Send()):
                return new Exception('Erro: ' . $mail->ErrorInfo);
            endif;
            return true;

        } catch (Exception $er)
        {
            return URL::er500();
        }
	}
}