<?php 
class catalog extends controller {
	 
	
	public static function _config(){
	}
	
	public static function setAction(){ return 'index'; }
	
	public static function index() {
		$category = URL::getCode(URL::uri());		
		$model = new modelProduct();
		$model->setCategory($category);
		$model->setMaker(isset($_GET['maker']) ? $_GET['maker'] : null);
		
		static::$data->header_search_input = isset($_GET['search']) && !empty($_GET['search']) ? $_GET['search'] : null;
		
		$model->setSearch(static::$data->header_search_input);
		$model->setFilters(isset($_GET['f']) ? json_decode(base64_decode($_GET['f'])) : array());
		$data = $model->getAll(URL::getVar('page'));
		
		foreach($data as $k=>$v)
			static::$data->{$k} = $v;
		
		static::$data->_title = !$data->category ? 'Todos os Produtos' : $data->category->name;
		static::_render('catalog.php');
	}
}