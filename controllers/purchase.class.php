<?php
require_once 'controller_ecommerce.class.php';
class purchase extends controller_ecommerce { 

	public static function _config(){
		parent::_config();
		static::$data->hide_btn_purchase = true;
	}
	
	// Tela Minhas Compras
	public static function all(){
		if(!defined('MY_ID'))
			return H::redirect(URL::root() . 'customer/index');
		
		$vars = new stdClass();
		$api = new API();	

		$current_page = URL::getVar('page');
		$limit = 10;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		
		$params = array('int_start'=>$start, 'int_limit'=>$limit, 'int_customer'=>MY_ID, 'vrc_name'=>null, 'vrc_status_alias'=>null);
		$api->addAction(static::$base,'ec_list_purchase','run', $params);
		static::$data->data = $api->callMethod()->data;
		
		$totalRecords = count((array)static::$data->data) ? current((array)static::$data->data)->total_records : 0;
		static::$data->pag = new Pagination($current_page, $limit, $totalRecords);
		
		static::_render('purchase/all.php');
	}
	
	// Visualização da compra
	public static function details(){	
		$path = 'files/comments';
		$errors = $params = array();
		
		static::$api->addAction(static::$base, 'ec_show_setting', 'run', array());
		$res = static::$api->callMethod();
		$settings = current($res->data);
		static::$data->deposit_acc = isset($settings->deposit_acc) ? $settings->deposit_acc : null;
		
		
		if(isset($_POST['comment'])):
			$params['purchase_comment_int_id'] = null;
			$params['purchase_comment_int_purchase'] = URL::friend(2);
			$params['purchase_comment_int_sender'] = MY_ID;
			$params['purchase_comment_tny_customer'] = 1;
			$params['purchase_comment_vrc_file'] = '';
			$params['purchase_comment_vrc_comment'] = $_POST['comment'];
			
						//UPLOAD
			$f = $_FILES['purchase_comment_vrc_file'];
			if(strlen($f['tmp_name']) > 0):
				$ext = pathinfo($f['name'], PATHINFO_EXTENSION);
				
				if(!in_array(strtolower($ext), array('jpg', 'jpeg', 'png', 'bmp', 'gif', 'pdf', 'txt'))):
					$errors[] = 'Extensão do arquivo inválida!';
				else:
					if(!file_exists($path)):
						$oldmask = umask(0);
						mkdir($path,0775);
						umask($oldmask);
					endif;
					$newFileName = sprintf('%s.%s', md5(uniqid()), $ext);
					move_uploaded_file($f['tmp_name'], $path . '/' . $newFileName );
				endif;
				$params['purchase_comment_vrc_file'] = $newFileName;
			endif;
			
			if(!count($errors)):
				static::$api->addAction(static::$base, 'ec_save_purchase_comment', 'run', $params);
				$res = static::$api->callMethod();
				$errors = $res->errors;
			endif;	
			if(!!$errors):
				return URL::er500();
			else:
				H::redirect('purchase','details', URL::friend(2));
			endif;	
		endif;
		
		
		modelPurchase::getInfo(URL::friend(2), static::$data);
		static::$data->path = URL::root() . $path;
		if(static::$data->purchase->purchase_status_vrc_alias == 'start'):
			H::redirect('purchase', 'step-4', URL::friend(2));
		else:
			static::$data->_content_mod = 'customer';
		static::_render('purchase/details.php');
		endif;
	}
	
	public static function cancel(){	
		$response = modelPurchase::setStatusByHash('canceled');
		if($response != 'SUCCESS'):
			header("HTTP/1.0 401 Not Authorized");
			header("Status: 401 Not Authorized");
			echo $response;
		else:
			H::redirect('purchase', 'details', URL::friend(2));
		endif;
	}
	
	
	// Confirmação do recebimento dos produtos
	public static function received(){
		$response = modelPurchase::setStatusByHash('received');
		if($response != 'SUCCESS'):
			header("HTTP/1.0 401 Not Authorized");
			header("Status: 401 Not Authorized");
			echo $response;
		else:
			H::redirect('purchase', 'details', URL::friend(2));
		endif;
	}
	
	// Confirmação do pagamento
	public static function deposited(){
		$response = modelPurchase::setStatusByHash('payment_accepted');
		if($response != 'SUCCESS'):
			header("HTTP/1.0 401 Not Authorized");
			header("Status: 401 Not Authorized");
			echo $response;
		else:
			H::redirect('purchase', 'details', URL::friend(2));
		endif;
	}
	
	// Confirmação automatica da compra
	public static function gateway_notification(){
		$response = modelPurchase::refreshStatusByGatewayNotification(static::$settings);
		
		if($response != 'SUCCESS'):
			header("HTTP/1.0 401 Not Authorized");
			header("Status: 401 Not Authorized");
			echo $response;
		else:
			header("HTTP/1.0 200 OK");
			header("Status: 200 OK");
			echo $response;
		endif;
	
	}
	
	// Compra - Passo 1 - Login
	public static function step_1(){
		if(defined('MY_ID'))
			H::redirect('purchase/step-2');
		
		$_GET['to'] = 'purchase-step-1';
		static::$data->fieldsRegister = static::fieldsRegister();
		static::$data->fieldsLogin = static::fieldsLogin();
		static::$data->_show_customer_menu = false;
		static::$data->step = 0;
		static::_render('purchase/step1.php');
	}
	
	// Compra - Passo 2 - Endereço
	public static function step_2(){
		if(!defined('MY_ID'))
			H::redirect('purchase/step-1');
		
		static::cartItems();
		static::addressData(true);
		
		static::$api->addAction(static::$base, 'ec_valid_setting', 'run', array());
		$res = static::$api->callMethod();
		$info = current($res->data);
		
		static::$data->errors = array();
		if($info->tny_payment_methods * 1 < 1):
			static::$data->errors[] = 'Nenhuma forma de pagamento está ativa.' . PHP_EOL;
		endif;
		
		if($info->tny_shipping_methods * 1 < 1):
			static::$data->errors[] = 'Nenhuma forma de entrega está ativa.' . PHP_EOL;
		endif;
		
		static::$data->_show_customer_menu = false;
		static::$data->hide_btn_purchase = true;
		static::$data->step = 1;
		static::_render('purchase/step2.php');
	}	
	
	// Compra - Passo 3 - Forma de Entrega
	public static function step_3(){
		if(!defined('MY_ID'))
			H::redirect('purchase/step-1');
		
		# Itens do carrinho
		static::cartItems();
		
		if(!static::$data->shopping_cart_data->items || !URL::friend(2))
			H::redirect('purchase/step-2');
		
		
		# Endereço selecionado
		static::singleAddress(URL::friend(2));
		

		static::$api->addAction(static::$base,'ec_list_payment','run', array('chr_language'=>'pt_br'));
		$res = static::$api->callMethod();
		$payment_methods = $res->data;
		static::getShipping();
		if(isset($_GET['shipping'])):
			$shippingMethod = $_GET['shipping'];
			
			
			$cartItems = modelCart::getItems();
			$cartTotal = 0;
			foreach($cartItems as &$i):
				$subtotal = $i->price * $i->quantity;
				$cartTotal +=  $subtotal;
			endforeach;
			
			static::$data->shipping_price = static::$data->info_methods[$shippingMethod][1];
			$label = static::$data->info_methods[$shippingMethod][0];
			$params = array(
				'purchase_int_customer' =>  MY_ID,
				'purchase_int_currency' =>  1,
				'purchase_int_shipping_address' => static::$data->address->address_int_id, 
				'purchase_dcm_subtotal' => $cartTotal,
				'purchase_dcm_shipping_price' => static::$data->shipping_price,
				'purchase_vrc_shipping_method' => $label,
				'chr_language'=>'pt_br'
			);
			static::$api->addAction(static::$base, 'ec_purchase_start', 'run', $params);
			$res = static::$api->callMethod();

			$errors = $res->errors;
			if(!!$errors)
				return URL::er500();
			
			$PurchaseData = current($res->data);
			$purchaseId = $PurchaseData->purchase_int_id;
			$purchaseHash = urlencode($PurchaseData->purchase_vrc_hash);

				
			static::$api->addAction(static::$base, 'ec_show_setting', 'run', array());
			$res_setting = static::$api->callMethod();
			$settings = current($res_setting->data);
			static::$data->deposit_acc = isset($settings->deposit_acc) ? $settings->deposit_acc : null;
			
			$goNext = URL::root() . 'purchase/step-5/' . $purchaseId . '/%s';
						
			foreach($payment_methods as $method):
				if($shippingMethod != 'pick_in_place' && $method->alias == 'pay_on_delivery'): continue;
				elseif($method->enabled * 1 < 1): continue;
				endif;
				if(in_array($method->alias, array('mercadopago','pagseguro'))):
					$checkoutUrl = modelPurchase::getCheckoutURL(
						static::$settings,
						$method->alias,
						$purchaseHash,
						$purchaseId,
						$cartItems, 
						static::$data->address, 
						$shippingMethod, 
						static::$data->shipping_price
					);
				elseif(in_array($method->alias,array('deposit', 'pay_on_delivery'))):
					$checkoutUrl = sprintf($goNext, $method->alias);
				endif;
				
				if(isset($checkoutUrl)):
					$params = array(
						'purchase_method_int_purchase' => $purchaseId,
						'purchase_method_vrc_method' => $method->alias,
						'purchase_method_vrc_url' => $checkoutUrl
					);
					static::$api->addAction(static::$base, 'ec_save_purchase_method', 'run', $params);
					$res = static::$api->callMethod();
					
					if(!!$res->errors):
						return URL::er500();
					endif;
					
				else:
					var_dump('error');
					die;
				endif;
			endforeach;
			H::redirect('purchase/step-4/'.$purchaseId);
		endif;
		static::$data->_show_customer_menu = false;
		static::$data->hide_btn_purchase = true;
		static::$data->step = 2;
		static::_render('purchase/step3.php');
	}	
	
	// Compra - Passo 4 - Forma de Pagamento
	public static function step_4(){
		if(!defined('MY_ID'))
			H::redirect('purchase/step-1');
		
		if(!URL::friend(2))
			H::redirect('purchase/step-2');
		
		modelPurchase::getInfo(URL::friend(2), static::$data);
		if(!static::$data->purchase):
			return URL::er500();
		endif;
		
		static::$data->_show_customer_menu = false;
		static::$data->hide_btn_purchase = true;
		static::$data->step = 3;
		
		static::_render('purchase/step4.php');
		
	}	
	
	// Compra - Passo 5 - Fim
	public static function step_5(){
		if(!defined('MY_ID'))
			H::redirect('purchase/step-1');
		
		if(!URL::friend(2))
			H::redirect('purchase/step-2');
		
		modelPurchase::getInfo(URL::friend(2), static::$data);
		if(!static::$data->purchase):
			return URL::er500();
		endif;
		
		$params = array(
			'purchase_int_id' => URL::friend(2),
			'purchase_int_customer' =>  MY_ID,
			'purchase_vrc_method'=>URL::friend(3)
			
		);
		static::$api->addAction(static::$base, 'ec_purchase_finish', 'run', $params);
		$res = static::$api->callMethod();

		$errors = $res->errors;
		if(!!$errors)
			return URL::er500();
		
		$response = current($res->data)->res;
		if($response == 'CLOSED'):
			H::redirect('purchase/purchase/' . URL::friend(2));
		elseif($response == 'NOT_FOUND'):
			var_dump($response);die;
		endif;
		
		static::$api->addAction(static::$base, 'ec_show_setting', 'run', array());
		$res = static::$api->callMethod();
		$settings = current($res->data);
		static::$data->deposit_acc = isset($settings->deposit_acc) ? $settings->deposit_acc : null;
		
		static::$data->_show_customer_menu = false;
		static::$data->hide_btn_purchase = true;
		static::$data->step = 4;
		static::_render('purchase/step5.php');
	}
		
}