<?php
require_once 'controller_ecommerce.class.php';
class cart extends controller_ecommerce { 

	// Exibir Carrinho drop down
	public static function header() {
		static::_render(null, 'layout/tpl-header-shipping-cart.php');
	}
	
	// Update Carrinho
	public static function cart_ajax() {
		static::index(true);
	}
	
	// Tela Carrinho
	public static function index($ajax = false) {
		static::cartItems();
		
		if($ajax):
			static::_render(null, 'cart/cart.php');
		else:
			H::js(array('cart.js'));
			static::_render('cart/index.php');
		endif;
	}
	
	// Operação Adicionar ao Carrinho
	public static function add() {
		if(!isset($_POST['product_dcm_quantity'])):
			echo json_encode(array('success'=>0, 'msg'=>'Dados inválidos.'));
			return true;
		endif;
		
		if(isset($_POST['product_options'])):
			foreach($_POST['product_options'] as $o):
				$params = array('cart_item_option_int_option_item'=>$o);
				static::$api->addAction(static::$base, 'ec_save_cart_item_option', 'run', $params);
			endforeach;
		endif;
		$customer_int_id = defined('MY_ID') ? MY_ID : null;
		$vrc_anonimous = CART_ID;
		$product_sub_int_id = URL::friend(2);
		$product_dcm_quantity = $_POST['product_dcm_quantity'];
		$params = compact(array('customer_int_id','vrc_anonimous','product_sub_int_id','product_dcm_quantity'));
		
		static::$api->addAction(static::$base, 'ec_cart_item_add', 'run', $params);
		$responses = static::$api->callMethodCombo();
		foreach($responses as $res):
			$errors = $res->errors;
			if(!!$errors):
				echo json_encode(array('success'=>0, 'msg'=>current($errors)));
				return true;
			endif;
		endforeach;
		$res = end($responses);
		$code = current($res->data)->res;
		$codes = array(
			'INVALID_DATA'=>'Dados inválidos!',
			'INVALID_QUANTITY'=>'Quantidade inválida!',
			'PRODUCT_NOT_FOUND'=>'Produto não encontrado.',
			'ALREADY_ADDED'=>'Este produto já foi adicionado ao carrinho.',
			'STOCK'=>'Estoque insuficiente.',
			'SUCCESS'=>'Produto adicionado com sucesso.'
		);
		echo json_encode(array('success'=> $code == 'SUCCESS', 'msg'=>$codes[$code]));
		return true;
	}
	
	// Operação Remover do Carrinho
	public static function remove(){
		
		$cart_item_bgn_id = URL::friend(2);
		if(empty($cart_item_bgn_id)):
			echo json_encode(array('success'=>0, 'msg'=>'Dados inválidos.'));
			return true;
		endif;
		$customer_int_id = defined('MY_ID') ? MY_ID : null;
		$vrc_anonimous = CART_ID;
		
		$params = compact(array('customer_int_id','vrc_anonimous','cart_item_bgn_id'));
		
		static::$api->addAction(static::$base, 'ec_cart_item_remove', 'run', $params);
		$res = static::$api->callMethod();
		
		$codes = array(
			'NOT_FOUND'=>'Item não encontrado!',
			'SUCCESS'=>'Produto removido com sucesso.'
		);
		$errors = $res->errors;
		if(!!$errors):
			echo json_encode(array('success'=>0, 'msg'=>current($errors)));
			return true;
		endif;
		$code = current($res->data)->res;
		if($code == 'SUCCESS'):
			static::index(true);
		else:
			echo json_encode(array('success'=> $code == 'SUCCESS', 'msg'=>$codes[$code]));
		endif;
		return true;
	}
}