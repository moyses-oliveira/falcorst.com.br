<?php
class product extends controller { 
	public static function _config(){
		
	}
	
	public static function setAction() { 
		if(URL::friend(1) == 'get-shipping.json')
			return 'get_shipping'; 
		
		return 'index'; 
	}

	public static function index(){
		H::css(array('css/product.css'));
		H::js(array('jquery.zoom.min.js', 'product.js'));
		$sid = URL::getCode(URL::uri());
		static::$data->sid = $sid;
		if(!$sid)
			return URL::er404();

		$model = new modelProduct();
		foreach($model->getOne($sid) as $k=>$v)
			static::$data->{$k} = $v;
		
		$desc = static::$data->desc;
		$img_data_list = static::$data->img_data_list;
		
		H::addMeta(array('property'=>'og:locale', 'content'=>'pt_BR'));
		H::addMeta(array('property'=>'og:url', 'content'=>$_SERVER['REQUEST_SCHEME'] . ':' . URL::atual()));
		H::addMeta(array('property'=>'og:title', 'content'=>$desc->product_description_vrc_name));
		H::addMeta(array('property'=>'og:site_name', 'content'=>TITLE . ' - ' . $desc->product_description_vrc_name));
		H::addMeta(array('property'=>'og:description', 'content'=>substr($desc->product_description_vrc_summary, 0, 250)));
		H::addMeta(array('property'=>'og:type', 'content'=>'website'));
		
		if(count($img_data_list)):
			$img = current($img_data_list)->img;
			$img_ext = pathinfo($img, PATHINFO_EXTENSION);
			$find = array('image/jpeg', 'image/jpeg', 'image/png', 'image/gif', 'image/bmp');
			$replace = array('jpeg', 'jpg', 'png', 'gif', 'bmp');
			$type = str_replace($find, $replace, strtolower($img_ext));

			$root_address = $_SERVER['REQUEST_SCHEME'] . ':' . URL::site();
			H::addMeta(array('property'=>'og:image', 'content'=> $root_address . PATH_IMAGES . $img));
			H::addMeta(array('property'=>'og:image:type', 'content'=>$type));
			H::addMeta(array('property'=>'og:image:width', 'content'=>'800')); /** PIXELS **/
			H::addMeta(array('property'=>'og:image:height', 'content'=>'600')); /** PIXELS **/
		endif;
		static::$data->_title = TITLE . ' - ' . $desc->product_description_vrc_name;
		static::_render('product.php');
	} 
	
	
	
	public static function get_shipping() {
		$cep = @$_GET['cep'];
		$id = @$_GET['product'];
		$qtt = @$_GET['qtt'];
		$success = false;
		$addr = new stdClass();
		$prices = array();
		if(in_array(null, array($qtt, $id, $qtt))):
			echo json_encode(array('success'=>$success, 'addr'=>$addr, 'prices'=>$prices));
			return;
		endif;
		
		$addr = (object)correio::endereco($cep);
		
		if(!isset($addr->uf)) {
			echo json_encode(array('success'=>$success, 'addr'=>new stdClass(), 'prices'=>$prices));
			return;
		}
		
		static::$api->addAction('ecommerce','ec_get_product_sub','run', array( 'product_sub_int_id' => $id ));
		static::$api->addAction('ecommerce','ec_get_product_sub_price_front','run', array( 'int_product_sub' => $id ));
		static::$api->addAction('ecommerce','ec_list_shipping','run', array('chr_language'=>'pt_br'));
		$response = static::$api->callMethodCombo();
		
		$prod = array_shift($response)->data[0];
		$price = @array_shift($response)->data[0];
		$shipping_methods = @array_shift($response)->data;
		
		static::$api->addAction('ecommerce','list_custom_shipping_by_zone_code','run', array( 'region_chr_zone'=>$addr->uf, 'dcm_weight'=>($prod->product_sub_dcm_weight * $qtt)));
		$custom_shipping_methods = static::$api->callMethod()->data;
		
		$prices = array();
		foreach($shipping_methods as $shipping):
			if(!$shipping->enabled) continue;
			
			if($shipping->alias == 'pick_in_place'):
				continue;
			elseif($shipping->alias == 'custom'):
				foreach($custom_shipping_methods as $csm):
					$cod = 'custom_'.$csm->regw_int_id;
					$label = sprintf('%s - %s - %s', $shipping->label, $addr->uf, $csm->region_vrc_name);
					$prices[$shipping->alias] = array($label, number_format($csm->regw_dcm_price, 2, '.', ''));
				endforeach;
			else:
				$value = 0;
				if(substr($shipping->alias,0,7) == 'correio'):
					$method = strtoupper(substr($shipping->alias,8));
					$cart = array(array());
					foreach(array('length', 'width', 'height') as $m)
						$cart[0][$m] = $prod->{'product_sub_dcm_' . $m} * 100;

					$cart[0]['weight'] = $prod->product_sub_dcm_weight;
					$cart[0]['price'] = $price->product_dcm_price;
					$cart[0]['quantity'] = $qtt;
					$cart[0]['key'] = $id;
					
					$packages = correio::makePackages($cart);
					
					$value = 0;
					$to = str_replace(array('.','-'), '', $cep);
					$from = static::getSettings()->post_cep;
					$from= str_replace(array('.','-'), '', $from);
					
					foreach($packages as $p):
						$value += correio::frete(constant($method), $from, $to, $p['weight'], $p['length'], $p['width'], $p['height'], $p['price']);
					endforeach;
				endif;

				if($value === false) continue;
				$prices[$shipping->alias] = array($shipping->label, number_format($value, 2, '.', ''));
			endif;
		endforeach;
		
		$success = true;
		echo json_encode(array('success'=>$success, 'addr'=>$addr, 'prices'=>$prices));
	}
}