<?php

class controller
{
    public static $cod = 0;
    public static $data = null;
    public static $api = null;
    protected static $settings = null; 
	
	public static function _config()
	{
		static::$data->title = '55 Digital';
		static::$data->headBg = H::root() . 'files/img/layout/devices.jpg';
	}
	
	protected static function getSettings() {
		if(!static::$settings):
			static::$api->addAction('ecommerce', 'ec_show_setting', 'run', array());
			$res = static::$api->callMethod();
			static::$settings = current($res->data);
		endif;
		return static::$settings;
	}

    public static function _render($file, $template = 'layout/tpl.php')
    {
        static::$data->_content_file = $file;
        $_data_ = static::$data;
        if (is_object($_data_))
            $_data_ = (array) $_data_;

        if (is_array($_data_))
            extract($_data_, EXTR_PREFIX_SAME, 'data');
        else
            $data = $_data_;

        require('views/' . $template);
    }

    public static function _redirect($l1, $l2 = null, $l3 = null, $l4 = null, $l5 = null, $l6 = null, $l7 = null)
    {
        header('Location: ' . URL::link($l1, $l2, $l3, $l4, $l5, $l6, $l7));
        exit();
    }

}
