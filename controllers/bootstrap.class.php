<?php
class bootstrap extends controller {
    public static function init()
    {
		static::$data = new stdClass();
		static::$data->_title = '';
		static::$data->_content_mod = 'normal';
		H::css(array(
			'css/bootstrap.min.css',
			'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,400,300,600,700,800',
			'http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700',
			'font-awesome/css/font-awesome.min.css',
			'css/owl.carousel.css',
			'css/magnific-popup.css',
			'css/jquery.share.css',
			'css/style.css',
			'css/responsive.css',			
			'css/boxes.css'
		));
		H::js(array(
			'jquery-1.11.1.min.js',
			'jquery-migrate-1.2.1.min.js',
			'bootstrap.min.js',
			'bootstrap-hover-dropdown.min.js',
			'jquery.magnific-popup.min.js',
			'jquery.share.js',
			'owl.carousel.min.js',
			'ShoppingCart.js',
			'form.js',
			'custom.js'
		));

		H::root(URL::root());
		$apiConf = json_decode(file_get_contents('files/configs/api.json'));
		foreach($apiConf as $k=>$v)
			define($k, $v);
			
		define('TITLE', 'Falcor Store');
		define('PATH_THUMBS', 'files/gallery/thumbs/');
		define('PATH_IMAGES', 'files/gallery/images/');
		// Informações de cliente e carrinho de compras
		static::$api = new API();
		
		dataEcommerce::load(static::$data);
		
		$settings = static::getSettings();
        
		list($module, $action, static::$cod) = array(URL::friend(0), URL::friend(1), URL::friend(2));
        
		if (!$module) $module = 'home';
			
		H::module($module);
		
		if(substr($module,-5) == '.html')
			$module = substr($module,0,-5);

        if (!$action) $action = 'index';
		H::action($action);
		
		H::addMeta(array('charset'=>'utf-8'));
		H::addMeta(array('name'=>'viewport', 'content'=>'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0'));
		H::addMeta(array('http-equiv'=>'X-UA-Compatible', 'content'=>'IE=edge'));
		H::addMeta(array('name'=>'theme-color', 'content'=>'#24bee8'));
		H::addMeta(array('type'=>'js', 'name'=>'ROOT', 'content'=>URL::root()));
		H::addMeta(array('type'=>'js', 'name'=>'URL_CURRENT', 'content'=>URL::atual()));
		H::addMeta(array('type'=>'js', 'name'=>'URL_MODULE', 'content'=>H::module()));
		H::addMeta(array('type'=>'js', 'name'=>'URL_ACTION', 'content'=>H::action()));
		H::addMeta(array('type'=>'js', 'name'=>'URL_AJAX_MINI_CART', 'content'=>URL::link('cart', 'header')));
		
		$class = str_replace('-','_',$module);
		$ctrlFile = sprintf('controllers/%s.class.php',$class);

		if(!file_exists($ctrlFile))
			static::_redirect('er404');
			
		require_once $ctrlFile;

        if (method_exists($class, '_config'))
            call_user_func($class . '::_config');
			
        if (method_exists($class, 'setAction'))
            $action = call_user_func($class . '::setAction');

		$action = str_replace('-','_',$action);
        if (!method_exists($class, $action))
            static::_redirect('er404');

        return call_user_func_array($class . '::' . $action, array());
    }
}