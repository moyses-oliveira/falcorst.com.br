<?php 
class home extends controller { 
	public static function _config()
	{
		static::$data->_title = null;
	}
	
	public static function index()
	{
		
		$model = new modelProduct();
		$model->enableFilterList(false);
		$model->enableMakerOptions(false);
		$model->setCategory(null);
		$model->setMaker(null);
		$model->setFilters(array());
		$data =$model->getAll(1);
		
		$model = new modelCMS();
		$model->findWeblogPost('banner-home');
		$banners = $model->getApi()->callMethod();
		$post_id = $banners->data[0]->post_int_id;
		
		static::$data->banner_home_imgs = modelWeblog::images($post_id);
		
		foreach($data as $k=>$v)
			static::$data->{$k} = $v;
		
		static::_render('home.php');
	}
}