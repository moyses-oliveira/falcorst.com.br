<?php 
require_once 'controller_ecommerce.class.php';
class customer extends controller_ecommerce { 
	
	// Tela de Login
	public static function index(){
		static::$data->errors = array();
		if(defined('MY_ID')):
			$params = array('int_customer'=>MY_ID, 'chr_language'=>'pt_br');
			static::$api->addAction(static::$base, 'ec_list_purchased_items', 'run', $params);
			$res = static::$api->callMethod();
			$errors = $res->errors;
			if(count($errors)) { var_dump($errors); die; }
			static::$data->list_product = $res->data;
			static::_render('customer/index.php');
			return true;
		endif;
		return static::login();
		
	}
	
	// Tela de Login
	public static function login(){
		static::$data->errors = array();
		static::$data->fieldsLogin = static::fieldsLogin();
		if(count($_POST)):
			static::validateFields(static::$data->fieldsLogin, static::$data->errors);
			if(!static::$data->errors):
				$params = $_POST;
				$params['vrc_session'] = session_id();
				$params['vrc_anonimous'] = CART_ID;
				static::$api->addAction(static::$base, 'ec_login', 'run', $params);
				$res = static::$api->callMethod();
				$confirmWarning = sprintf('%s:%scustomer/reconfirm/?email=%s',$_SERVER['REQUEST_SCHEME'],URL::site(),urlencode($_POST['vrc_email']));
				$error_code = array(
					'NOT_FOUND'=>'Usuário não encontrado.',
					'PASSWORD'=>'Senha incorreta.',
					'CONFIRM'=>'<span id="confirm-warning">Você ainda não confirmou seu cadastro, acesse o link que foi enviado por e-mail para confirmar ou <a id="retry-confirm" href="' . $confirmWarning . '">clique aqui para receber outro e-mail de confirmação.</a></span>'
				);
				
				static::$data->errors = array();
				if(!$res->errors):
					$inf = current($res->data)->res;
					if(substr($inf,0,1) == 'E'):
						static::$data->errors[] = $error_code[substr($inf,2)];
					else:
						$pk = substr($inf,2);
						$params = array('customer_int_id'=>$pk);
						static::$api->addAction(static::$base,'ec_get_customer','run', $params);
						$_SESSION['customer'] = static::$api->callMethod()->data[0];
					endif;
				else:
					static::$data->errors = $res->errors;
				endif;
			endif;
		endif;
		
		if(count($_POST))
			return static::_render(null, 'customer/form-login.php');
		
		return static::_render('customer/login.php');
	}
	
	// Tela de Cadastro
	public static function register(){
		$confirm = 'customer/form-confirm.php';
		static::$data->errors = array();
		
		static::$data->fieldsRegister = static::fieldsRegister();
		if(count($_POST)):
			static::validateFields(static::$data->fieldsRegister, static::$data->errors);			
			if(!static::$data->errors): 
				$data = array();
				foreach(static::$data->fieldsRegister as $k=>$v)
					$data['customer_'.$k] = $v->true_value;
				
				$data['vrc_action'] = 'register';
				
				static::$api->addAction(static::$base, 'ec_save_customer', 'run', $data);
				$res = static::$api->callMethod();
				static::$data->errors = $res->errors;
				
				if(!static::$data->errors):
					$msg = $res->data[0]->message;
					if($msg != 'SUCCESS'):
						$msgs = array(
							'ALREADY_EXISTS'=>'Este e-mail já está cadastrado.',
							'UNDEFINED'=>'Ação não identificada'
						);
						static::$data->errors[] = $msgs[$msg];
					else:
						static::ConfirmationMail($data['customer_vrc_email'], static::$data->errors);
					endif;
				endif;
			endif;
		endif;

		if(count($_POST)):
			if(!static::$data->errors):
				return static::_render(null, 'customer/register_confirm.php');
			else:
				return static::_render(null, 'customer/form-register.php');
			endif;
		endif;
		
		return static::_render('customer/register.php');
	}
	
	
	// Atualização da senha
	public static function refresh_password(){
		$errors = array();
		if(isset($_POST['vrc_email'])):
			/* Pega as informações sobre o cliente atravéz do e-mail */
			static::$api->addAction(static::$base, 'ec_get_customer_by_email', 'run', $_POST);
			$res = static::$api->callMethod();
			$errors = $res->errors;
			
			if(!$errors):
				$customer = current($res->data);
				if(!$customer)
					$errors[] = 'Cliente não encontrado';
			endif;
			
			if(!$errors):
				static::$api->addAction(static::$base, 'ec_refresh_password', 'run', $_POST);
				$resPass = static::$api->callMethod();
				$errors = $res->errors;
			endif;
			
			if(!$errors):
				$password = current($resPass->data)->res;
				if(substr($password,0,1) == 'E')
					$errors[] = 'Cliente não encontrado';
					
				$password = substr($password,2);
			endif;
			
			/* Envia o e-mail com a nova senha */
			if(!$errors):
				$siteLink = sprintf('%s:%s',$_SERVER['REQUEST_SCHEME'],URL::site());
				
				$logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNgYAAAAAMAASsJTYQAAAAASUVORK5CYII=';
				$siteName = static::getSettings()->smtp_name;
				
				$tplFile = 'views/customer/refresh-password.mail.html';
				$tplData = compact('logo','siteName', 'siteLink', 'password');
				$subj = 'Nova senha gerada pelo sistema';
				static::MailFromTemplate(array(array($customer->email,$customer->name)), $subj, $tplFile, $tplData);
			endif;
			if(!$errors):
				echo json_encode(array('response'=>'Uma nova senha foi enviada para seu e-mail.'));
			else:
				echo json_encode(array('response'=>current($errors)));
			endif;
		else:
			echo json_encode(array('response'=>'E-mail não declarado.'));
		endif;
	}
	
	// Reenvio do e-mail de confirmação de cadastro
	public static function reconfirm(){
		$errors = array();
		if(isset($_GET['email'])):
			static::ConfirmationMail($_GET['email'], $errors);
			if(!$errors):
				return static::_render(null, 'customer/register_confirm.php');
			else:
				echo json_encode(array('response'=>current($errors)));
			endif;
		else:
			echo json_encode(array('response'=>'E-mail não declarado.'));
		endif;
	}
	
	// Confirmação de cadastro
	public static function confirm(){
		
		$confirm_code = @$_REQUEST['confirm_code'];
		$confirm_pass = @$_REQUEST['confirm_pass'];
		$confirm_email = null;
		if(!empty($confirm_pass) && $confirm_pass == @$_SESSION['confirm_pass']):
			$confirm_email = $_SESSION['confirm_email'];
		endif;
		
		if(empty($confirm_code) && empty($confirm_pass))
			return URL::er404();
		
		$params['confirm_code'] = $confirm_code ? $confirm_code : null;
		$params['email'] = $confirm_email ? $confirm_email : null;
		$params['session'] = session_id();
		$params['vrc_anonimous'] = defined('CART_ID') ? CART_ID : null;
		static::$api->addAction(static::$base, 'ec_confirm', 'run', $params);
		$res = static::$api->callMethod();
		$error_code = array(
			'NOT_FOUND'=>'Código de confirmação inválido.',
			'CONFIRMED'=>'Este usuário já foi confirmado.'
		);
		static::$data->errors = array();
		if(!$res->errors):
			$resData = current($res->data);
			if(isset($resData->message)):
				static::$data->errors[] = $error_code[$resData->message];
			else:
				$_SESSION['customer'] = $resData;
			endif;
		else:
			static::$data->errors = $res->errors;
		endif;
		if(!empty($confirm_email)):
			return static::_render(null, 'customer/confirm.php');
		endif;
		static::_render('customer/confirm.php');
	}
		
	// Tela de Alteração de Senha
	public static function password() {
		$fields = array();
		$fields['vrc_pass'] = (object)array('label'=>'Senha atual','type'=>array('not_null', 'pass'),'value'=>'', 'true_value'=>'');
		$fields['vrc_pass_new'] = (object)array('label'=>'Nova senha','type'=>array('not_null', 'pass'),'value'=>'', 'true_value'=>'');
		$fields['vrc_pass_repeat'] = (object)array('label'=>'Repita a nova senha','type'=>array('not_null', 'pass'),'value'=>'', 'true_value'=>'');
		static::$data->fields = $fields;
		static::$data->errors = array();
		
		
		if(count($_POST)):
			static::validateFields(static::$data->fields, static::$data->errors);			
			if(!static::$data->errors): 
				$data = array();
				foreach(static::$data->fields as $k=>$v)
					$data[$k] = $v->true_value;
				
				$data['vrc_email'] = MY_EMAIL;
				
				static::$api->addAction(static::$base, 'ec_update_password', 'run', $data);
				$res = static::$api->callMethod();
				static::$data->errors = $res->errors;
				
				if(!static::$data->errors):
					$response = current($res->data)->res;
					if(substr($response,0,1) != 'S'):
						$error_code = array(
							'NOT_FOUND'=>'Usuário não encontrado.',
							'INVALID'=>'A senha atual não confere.',
							'DIFERENT'=>sprintf(
								'Os campos <strong>%s</strong> e <strong>%s</strong> são diferentes.',
								$fields['vrc_pass_new']->label, $fields['vrc_pass_repeat']->label
							)
						);
						static::$data->errors[] = $error_code[substr($response, 2)];
					endif;
				endif;
			endif;
		endif;
		
		static::_render('customer/password.php');
	}
	
	// Tela Perfil
	public static function address(){ 
		if(!defined('MY_ID'))
			return URL::er404();
			
		static::addressData();
		static::_render('customer/address.php');
	}
	
	// Operação Logout
	public static function logout(){
		session_destroy();
		H::redirect(H::site().'home');
	}

	public static function get_shipping(){
		$cep = $_GET['cep'];
		$addr = (object)correio::endereco($cep);
		if(!$addr):
			echo json_encode(array('success'=>false, 'addr'=>new stdClass(), 'prices'=>array()));
			return;
		endif;
		
		static::getShipping($addr);
		foreach(static::$data->info_methods as &$i) $i[1] = 'R$ '. number_format($i[1] * 1, 2,',','');
		
		echo json_encode(array('success'=>true, 'addr'=>$addr, 'prices'=>static::$data->info_methods));
	}
	
	public static function get_cep(){
		$cep = $_GET['cep'];
		$addr = (object)correio::endereco($cep);
		echo json_encode($addr);
	}
}
